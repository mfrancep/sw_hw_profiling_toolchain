#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>

#include <errno.h>    //for error handling

#include "structs.h"
#include "tools.h"

/* extern variables */
extern std::ofstream dyn_file;
extern std::ofstream fail_file;
extern std::vector <struct addresses> addresses;
extern uint8_t program_current_index;
extern uint8_t core_number;
extern std::map <uint32_t, locks_info_t> locks_info;

/* local variables */
static uint32_t curr_lock = 0;
/* current_locks => key : fp  /  data : vector of lock */
static std::map <uint32_t, std::vector<signed_data_t> > current_locks;
/* mutex_in_times => key : fp  / data : time */
static std::map <uint32_t, signed_data_t> mutex_in_times; // map is used to save time during search phases
/* ret_addresses => key : fp / data : @ return */
static std::map <uint32_t, signed_data_t> ret_addresses; // map is used to store lock return addresses
/* sleeping_threads => key : fp / data : none */
static std::map <uint32_t, signed_data_t> sleeping_threads; // map is used to save time during search phases
/* release_times => key : @ lock / data : time */
static std::map <uint32_t, uint32_t> release_times; // map is used to save time during search phases
static std::map <uint32_t, uint32_t> pending_ident; // map is used to track the identification phase (useful to identify recursive lock)
static std::map <uint32_t, fairness_cnt_t> fairness_eval; // map is used to track the lock fairness. It is composed of TID and fairness counter (tid1:cnt, tid2:cnt, ...)
static signed_data_t timestamp;

void analyse_lock(uint8_t proc, core_signals_t &l_data_signal, delays_t &delay)
{	
	/***********************************************************************************************/
	/*                                       lock analysis                                         */
	/***********************************************************************************************/
	/* enter in the lock function */
	if (l_data_signal.pc == addresses[program_current_index].mutex_lock_addr)
	{
		debug_print("Analysis : %X==%X => enter in lock function (proc %d)\n", l_data_signal.pc, addresses[program_current_index].mutex_lock_addr, proc);
		char str[2048];
		sprintf(str,"Analysis : %X==%X => enter in lock function (proc %d)\n", l_data_signal.pc, addresses[program_current_index].mutex_lock_addr, proc);
		write_debug(l_data_signal.r29, str);
			
		timestamp.tid.gp = l_data_signal.r28;
		timestamp.tid.fp = l_data_signal.r29;
		timestamp.data = l_data_signal.clock;
		mutex_in_times[timestamp.tid.fp] = timestamp;

		debug_print("insert mutex_in_times[%X] = %X / %d\n", timestamp.tid.fp, mutex_in_times[timestamp.tid.fp].data, key_exist(mutex_in_times, timestamp.tid.fp ));

		signed_data_t return_addr;
		return_addr.tid = timestamp.tid;
		return_addr.data = l_data_signal.r31;
		ret_addresses[return_addr.tid.fp] = return_addr;

		//waiting for lock identification
		pending_ident[l_data_signal.r29] = 1;

		//dynamic trace
		dyn_file << l_data_signal.clock << ";0;" << (uint32_t)proc << ";" <<  l_data_signal.r29 << ";;;;;;\n";
	}
	// return from the lock function
	else if ((key_exist(ret_addresses,  l_data_signal.r29)) &&  
		 (ret_addresses[l_data_signal.r29].data == l_data_signal.pc) &&
		 (ret_addresses[l_data_signal.r29].tid.gp == l_data_signal.r28))
	{
		//dynamic trace
		dyn_file << l_data_signal.clock << ";3;" << (uint32_t)proc << ";" <<  l_data_signal.r29 << ";";
			
		debug_print("Analysis : %X==%X => return from lock function (proc %d)\n", ret_addresses[l_data_signal.r29].data, l_data_signal.pc, proc);
			
		/* clear return address entry since we are not anymore in the function */
		ret_addresses.erase(l_data_signal.r29);

		/* lock contention decrementation */
		if ((key_exist_uint32(pending_ident,  l_data_signal.r29)) &&
		    (pending_ident[l_data_signal.r29] == 0)  &&
		    (key_exist_vector_item(current_locks, l_data_signal.r29)))
		{
			// lock identification occured
			locks_info[current_locks[l_data_signal.r29].back().data].contention--;
			debug_print("Analysis : decremente lock contention %X contention %d\n", current_locks[l_data_signal.r29].back().data, locks_info[current_locks[l_data_signal.r29].back().data].contention);

			//dynamic trace (@lock)
			dyn_file << (uint32_t) current_locks[l_data_signal.r29].back().data << ";";
			//dynamic trace (contention)
			dyn_file << (uint32_t) locks_info[current_locks[l_data_signal.r29].back().data].contention <<  ";";

			curr_lock = current_locks[l_data_signal.r29].back().data;
		}
		else
		{
			/* non identified lock : discard computing */
			/* should occured if recusif lock are used */
			debug_print("Analysis : non identified lock (recursive lock)\n");
			//dynamic trace (@lock ; contention)
			dyn_file << ";;;;;;\n";
			return;
		}
		pending_ident.erase(l_data_signal.r29);

		/* compute acquire delay */
		// retrive in function timestamps
		if ((key_exist(mutex_in_times,  l_data_signal.r29)) &&
		    (mutex_in_times[l_data_signal.r29].tid.gp == l_data_signal.r28) &&
		    !(key_exist(sleeping_threads, l_data_signal.r29))) // we store only non sleeping process
		{
			//dynamic trace (acquire delay)
			dyn_file << (uint32_t)(l_data_signal.clock - mutex_in_times[l_data_signal.r29].data) << ";";
				
			debug_print("Analysis : acquire delay compute\n");
			delay.locks[curr_lock].acquire_delays.push_back(l_data_signal.clock - mutex_in_times[l_data_signal.r29].data);
			// update mutex_in_time to critical section compute
			mutex_in_times[l_data_signal.r29].data = l_data_signal.clock;
			// clear in function timestamps entry of the map
			//mutex_in_times.erase(l_data_signal.r29);
		}
		else
		{
			//dynamic trace (acquire delay)
			dyn_file << ";";
		}

		/* release delays compute */
		if ((key_exist_vector_item(current_locks, l_data_signal.r29)) &&
		    (key_exist_uint32(release_times, current_locks[l_data_signal.r29].back().data)))
		{
			debug_print("Analysis : release delay compute\n");
				
			/* does the threads go through waiting (sleeping process) ? */
			if (key_exist(sleeping_threads, l_data_signal.r29))
			{
				debug_print("Analysis : previously sleeping %X\n", l_data_signal.r29);
				// delete map entry
				sleeping_threads.erase(l_data_signal.r29);
				delay.locks[curr_lock].contented_release_delays.push_back(l_data_signal.clock - release_times[current_locks[l_data_signal.r29].back().data]);
				// update mutex_in time for critical section computation
				timestamp.tid.gp = l_data_signal.r28;
				timestamp.tid.fp = l_data_signal.r29;
				timestamp.data = l_data_signal.clock;
				mutex_in_times[timestamp.tid.fp] = timestamp;

				//dynamic trace (contented release delay)
				dyn_file << (uint32_t)(l_data_signal.clock - release_times[current_locks[l_data_signal.r29].back().data]) << ";";
			}
			else
			{
				delay.locks[curr_lock].interlock_delays.push_back(l_data_signal.clock - release_times[current_locks[l_data_signal.r29].back().data]);
				//dynamic trace (contented release delay)
				dyn_file << ";";
			}
			// delete the entry from the map
			release_times.erase(current_locks[l_data_signal.r29].back().data);
		}
		else
		{
			//dynamic trace (contented release delay)
			dyn_file << ";";
		}

		//dynamic trace (go to sleep ; critical section)
		dyn_file << ";;";

		//fairness evaluation
		for (std::map<uint32_t, fairness_cnt_t>::iterator it=fairness_eval.begin(); it!=fairness_eval.end(); ++it)
		{
			if ((it->first != l_data_signal.r29) && (it->second.lock_id == curr_lock))// other threads bypass pending thread
				it->second.count++;
			else if ((it->first == l_data_signal.r29) && (it->second.lock_id == curr_lock))// the thread returning from the lock was in the fairness list (previously sleeping)
			{
				dyn_file << it->second.count;
				printf("fairness : %d\n", it->second.count);
				if (it->second .count> core_number)
					printf("warning : lock unfaire!\n");
				// delete the entry
				fairness_eval.erase(it->first);
			}
		}

		//dynamic trace (end off line)
		dyn_file << "\n";

		/*
		if (wakeup_in_time_set_lock_flag)
		{
			delay.locks[curr_lock].wakeup_durations.push_back(l_data_signal.clock-wakeup_in_time[proc]);
			wakeup_in_time_set_lock_flag = false;
		}
		*/
	}
	// go to sleep
	else if ((l_data_signal.pc == addresses[program_current_index].lock_wait_addr) ||
		 (l_data_signal.pc == addresses[program_current_index].lock_wait_priv_addr))
	{
		//dynamic trace
		dyn_file << l_data_signal.clock << ";2;" << (uint32_t)proc << ";" <<  l_data_signal.r29 << ";";
			
		debug_print("Analysis : %X==%X/%X go to sleep (proc %d) \n", l_data_signal.pc, addresses[program_current_index].lock_wait_addr, addresses[program_current_index].lock_wait_priv_addr, proc);
			
		char str[2048];
		sprintf(str,"Analysis : %X==%X/%X go to sleep (proc %d) \n", l_data_signal.pc, addresses[program_current_index].lock_wait_addr, addresses[program_current_index].lock_wait_priv_addr, proc);
		//write_debug(l_data_signal.r29, str);

		if (!key_exist_vector_item(current_locks, l_data_signal.r29))
		{
			/* non identified lock : discard computing */
			/* should occured if recusif lock are used */
			debug_print("Analysis : non identified lock (recursive lock)\n");
			//dynamic trace
			dyn_file <<";;;;;\n";
			return;
		}
			
		curr_lock =  current_locks[l_data_signal.r29].back().data;

		//dynamic trace (@lock)
		dyn_file << (uint32_t) current_locks[l_data_signal.r29].back().data << ";";
		//dynamic trace (contention ; acquire delay ; contented release delay)
		dyn_file << (uint32_t) locks_info[current_locks[l_data_signal.r29].back().data].contention <<  ";;;";

		// ! we stiil are in the funtion
		/* clear return address entry since we are not anymore in the function */
		//if (key_exist(ret_addresses,  l_data_signal.r29))
		//	ret_addresses.erase(l_data_signal.r29);

		/* compute go to sleep delay */
		// retrieve in function timestamps
		if ((key_exist(mutex_in_times,  l_data_signal.r29)) && (mutex_in_times[l_data_signal.r29].tid.gp == l_data_signal.r28))
		{
			// failed call file
			fail_file << (uint32_t)mutex_in_times[l_data_signal.r29].data << ";" << (uint32_t)l_data_signal.r29 << "\n";
				
			//dynamic trace (go to sleep ; critical section)
			dyn_file << (uint32_t) (l_data_signal.clock - mutex_in_times[l_data_signal.r29].data) << ";\n";
			
			debug_print("Analysis : go to sleep delay computation \n");
			delay.locks[curr_lock].go_to_sleep_delays.push_back(l_data_signal.clock - mutex_in_times[l_data_signal.r29].data);
			// clear in function timestamps entry of the map
			mutex_in_times.erase(l_data_signal.r29);
		}
		else
		{
			//dynamic trace (go to sleep ; critical section)
			dyn_file << ";\n";
		}
			
		// store thread id in sleeping thread map
		signed_data_t tid;
		tid.tid.gp = l_data_signal.r28;
		tid.tid.fp = l_data_signal.r29;
		sleeping_threads[tid.tid.fp] = tid;

		fairness_cnt_t f;
		f.lock_id = curr_lock;
		f.count = 1;
		fairness_eval[l_data_signal.r29] = f;
	}
	// critical section duration
	else if (l_data_signal.pc == addresses[program_current_index].mutex_unlock_addr)
	{
		//dynamic trace
		dyn_file << l_data_signal.clock << ";4;" << (uint32_t)proc << ";" <<  l_data_signal.r29 << ";";

		if (key_exist_vector_item(current_locks, l_data_signal.r29))
		{
			//dynamic trace (@lock)
			dyn_file << (uint32_t) current_locks[l_data_signal.r29].back().data << ";;;;;";
			curr_lock = current_locks[l_data_signal.r29].back().data;
		}
		else
		{
			//dynamic trace (@lock)
			dyn_file << ";;;;;";
		}
			
		debug_print("Analysis : %X==%X critical section (proc %d) \n", l_data_signal.pc, addresses[program_current_index].mutex_unlock_addr, proc);
		char str[2048];
		sprintf(str,"Analysis : %X==%X critical section (proc %d) \n", l_data_signal.pc, addresses[program_current_index].mutex_unlock_addr, proc);
		write_debug(l_data_signal.r29, str);
			
		/* clear return address entry since we are not anymore in the function */
		if (key_exist(ret_addresses,  l_data_signal.r29))
			ret_addresses.erase(l_data_signal.r29);
		/* compute critical section duration */
		// retrive in function timestamps
		debug_print("mutex_in_times[%X] = %X / %d\n", l_data_signal.r29, mutex_in_times[l_data_signal.r29].data, key_exist(mutex_in_times,  l_data_signal.r29));
		if ((key_exist(mutex_in_times,  l_data_signal.r29)) && (mutex_in_times[l_data_signal.r29].tid.gp == l_data_signal.r28))
		{
			//dynamic trace (@critical section)
			dyn_file << (uint32_t) (l_data_signal.clock - mutex_in_times[l_data_signal.r29].data) <<"\n";
				
			debug_print("Analysis : critical section computation %d - %d = %d \n", l_data_signal.clock, mutex_in_times[l_data_signal.r29].data, (l_data_signal.clock - mutex_in_times[l_data_signal.r29].data));
			delay.locks[curr_lock].cs_times.push_back(l_data_signal.clock - mutex_in_times[l_data_signal.r29].data);
			// clear in function timestamps entry of the map
			mutex_in_times.erase(l_data_signal.r29);
		}
		else
		{
			//dynamic trace (@critical section)
			dyn_file << "\n";
		}
			
		// save release time
		if (key_exist_vector_item(current_locks, l_data_signal.r29))
		{
			debug_print("Analysis : save release time for fp %u \n", l_data_signal.r29);
			release_times[current_locks[l_data_signal.r29].back().data] = l_data_signal.clock;
			// get the current lock entry
			// we assume that in nested lock the first lock to be released is the last acquire 
			current_locks[l_data_signal.r29].pop_back();
			debug_print("current_locks[%X].size = %d\n", l_data_signal.r29, current_locks[l_data_signal.r29].size());
			if (current_locks[l_data_signal.r29].size() == 0)
				current_locks.erase(l_data_signal.r29);
		}   
	}
	// lock identification
	else if ((l_data_signal.pc == addresses[program_current_index].ll_instr_addr) ||
		 (l_data_signal.pc == addresses[program_current_index].ll_instr_addr_2)) 
	{
		debug_print("Analysis : %X==%X lock identification (proc %d) \n", l_data_signal.pc, addresses[program_current_index].ll_instr_addr, proc);
		char str[2048];
		sprintf(str,"Analysis : %X==%X lock identification (proc %d) \n", l_data_signal.pc, addresses[program_current_index].ll_instr_addr, proc);
		write_debug(l_data_signal.r29, str);
			
		// we implement a vector of current lock form each tid to manage nested lock
		signed_data_t lock;
		lock.tid.gp = l_data_signal.r28;
		lock.tid.fp = l_data_signal.r29;
		lock.data = l_data_signal.r6;
		// the ll instruction could be in a loop is case of cs instruction failed
		// so we have to prevent from multiple adding
		if ((!key_exist_vector_item(current_locks, l_data_signal.r29)) &&
		    (!signed_item_exist_in_vector(current_locks[l_data_signal.r29], lock)))
		{
			debug_print("Analysis : add current lock %X to fp %X\n", lock.data, lock.tid.fp);
			current_locks[l_data_signal.r29].push_back(lock);
				
			if (key_exist_lock(locks_info, l_data_signal.r6))
			{
				locks_info[l_data_signal.r6].contention++;
				debug_print("Analysis : add current lock %X contention %d\n", lock.data, locks_info[l_data_signal.r6].contention);
			}
			else
			{
				locks_info_t lockinfo;
				lockinfo.contention = 1;
				lockinfo.reused_locking = 0;
				lockinfo.total_locking = 0;
				locks_info[l_data_signal.r6] = lockinfo;
				debug_print("Analysis : [new] add current lock %X contention %d\n", lock.data, locks_info[l_data_signal.r6].contention);
			}

			// computre re-use ratio
			if ((locks_info[l_data_signal.r6].dyn.size() > 0) && (proc == locks_info[l_data_signal.r6].dyn.back().proc))
				locks_info[l_data_signal.r6].reused_locking++;
			locks_info[l_data_signal.r6].total_locking++;
			
			// dynamic lock trace currently the request order
			// if we want to trace lock real acquisition we have to move the following code in the "return function" section 
			struct lock_dyn l_dyn;
			l_dyn.time_req = l_data_signal.clock;
			l_dyn.proc = proc;
			l_dyn.tid = l_data_signal.r29;
			locks_info[l_data_signal.r6].dyn.push_back(l_dyn);

			//dynamic trace
			dyn_file << l_data_signal.clock << ";1;" << (uint32_t)proc << ";" <<  l_data_signal.r29 << ";" << l_data_signal.r6 << ";";
			dyn_file << locks_info[l_data_signal.r6].contention << ";;;;\n";
		}
			
		//lock identification acquitement
		pending_ident[l_data_signal.r29] = 0;
	}
	//printf("current size locks %d\n", delay.locks.size());
}
