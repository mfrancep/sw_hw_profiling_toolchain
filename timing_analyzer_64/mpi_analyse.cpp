#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>

#include <errno.h>    //for error handling

#include "structs.h"
#include "tools.h"

// comment the following line if uname function are in the barrier wakening process
//#define ABSOLUTE_UNAME_FCT

#ifdef MPI_EVAL

/* extern variables */
extern std::vector <struct addresses> addresses;
extern uint8_t program_current_index;
extern uint8_t core_number;
extern uint32_t *  wakeup_in_time;
extern uint32_t * fct_uname_1_in_time;
extern uint32_t * fct_uname_2_in_time;
extern uint32_t * fct_uname_3_in_time;
extern uint8_t * index_barrier_counter;
extern uint8_t * wakeup_in_time_set_barrier_flags;
extern uint8_t * round_cnt;
extern std::vector <uint32_t> * messages_exchanges_intra_durations;
extern std::vector <uint32_t> * messages_exchanges_inter_durations;
extern uint32_t * start_echange_time;
extern int * budy_proc_tab;

/* local variables */

static signed_data_t return_addr;
/* ret_wake_futex => key : fp / data : @ return */
static std::map <uint32_t, signed_data_t> ret_wake_futex; // map used to store futex wake return addresses
/* ret_fct_uname_1 => key : fp / data : @ return */
static std::map <uint32_t, signed_data_t> ret_fct_uname_1; // map used to store uname fct 1 return addresses
/* ret_fct_uname_2 => key : fp / data : @ return */
static std::map <uint32_t, signed_data_t> ret_fct_uname_2; // map used to store uname fct 2 return addresses
/* ret_fct_uname_3 => key : fp / data : @ return */
static std::map <uint32_t, signed_data_t> ret_fct_uname_3; // map used to store uname fct 3 return addresses

static uint32_t last_arrival_time[2];



void analyse_mpi(uint8_t proc, core_signals_t &l_data_signal, delays_t &delay)
{
	/***********************************************************************************************/
	/*                                                barrier analysis                                              */
	/***********************************************************************************************/
	if (l_data_signal.pc == addresses[program_current_index].futex_wake_addr)
	{
		debug_print("Analysis : %X==%X => barrier function (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].futex_wake_addr, proc);
		return_addr.tid.gp = l_data_signal.r28;
		return_addr.tid.fp = l_data_signal.r29;
		return_addr.data = l_data_signal.r31;
		ret_wake_futex[return_addr.tid.fp] = return_addr;

		wakeup_in_time[proc] =  l_data_signal.clock;
		wakeup_in_time_set_barrier_flags[proc] = 1;
		printf("wakeup time set to %u\n", wakeup_in_time[proc]);

		last_arrival_time[index_barrier_counter[proc]] = wakeup_in_time[proc];
		delay.barrier.arrival_times.push_back(l_data_signal.clock);
		
		/*
		for(int i = 0; i<core_number; i++)
			printf("index_barrier_counter[%d]=%d\n", i, index_barrier_counter[i]);
		*/
		round_cnt[proc] = 0;
	}
	// return from the futex wake function
	else if ((key_exist(ret_wake_futex,  l_data_signal.r29)) &&  
		 (ret_wake_futex[l_data_signal.r29].data == l_data_signal.pc) &&
		 (ret_wake_futex[l_data_signal.r29].tid.gp == l_data_signal.r28))
	{
		debug_print("Analysis : %X==%X => return from barrier function (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].futex_wake_addr, proc);
		/* clear return address entry since we are not anymore in the function */
		ret_wake_futex.erase(l_data_signal.r29);

		if (wakeup_in_time_set_barrier_flags[proc])
		{
			delay.barrier.wakeup_times.push_back(l_data_signal.clock);
			delay.barrier.wakeup_durations.push_back(l_data_signal.clock-last_arrival_time[index_barrier_counter[proc]]);
			debug_print("Analysis : wakeup %d\n", (l_data_signal.clock-last_arrival_time[index_barrier_counter[proc]]));
			debug_print("wup last_arrival_time[%d [%d]] = %d\n", index_barrier_counter[proc], proc, last_arrival_time[index_barrier_counter[proc]]);
			index_barrier_counter[proc] = (index_barrier_counter[proc]+1)%2;
				
			wakeup_in_time_set_barrier_flags[proc] = 0;
		}
	}
	else if (l_data_signal.pc == addresses[program_current_index].fct_uname_1_addr)
	{
		debug_print("Analysis : %X==%X => fct uname 1 (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].fct_uname_1_addr, proc);
		return_addr.tid.gp = l_data_signal.r28;
		return_addr.tid.fp = l_data_signal.r29;
		return_addr.data = l_data_signal.r31;
		ret_fct_uname_1[return_addr.tid.fp] = return_addr;

		debug_print("ret_fct_uname_1[%d]\n", return_addr.tid.fp);

		fct_uname_1_in_time[proc] = l_data_signal.clock;

		// store time as start echange point
		start_echange_time[proc] = l_data_signal.clock;
		uint8_t mask = 0x1;
		mask = mask << round_cnt[proc];
		int budy = proc ^ mask;
		debug_print("start echange time proc %d = %d, post for budy %d in round %d\n", proc, start_echange_time[proc], budy, round_cnt[proc]);
	}
	// return from the uname function 1
	else if ((key_exist(ret_fct_uname_1,  l_data_signal.r29)) &&  
		 (ret_fct_uname_1[l_data_signal.r29].data == l_data_signal.pc) &&
		 (ret_fct_uname_1[l_data_signal.r29].tid.gp == l_data_signal.r28))
	{
		debug_print("Analysis : %X==%X => return from fct uname 1 (proc %d) (time spent %d flag %d )\n", l_data_signal.pc,  addresses[program_current_index].fct_uname_1_addr, proc, (l_data_signal.clock-fct_uname_1_in_time[proc]), wakeup_in_time_set_barrier_flags[proc]);
		/* clear return address entry since we are not anymore in the function */
		ret_fct_uname_1.erase(l_data_signal.r29);
			
#ifndef ABSOLUTE_UNAME_FCT
		if (wakeup_in_time_set_barrier_flags[proc])
#endif
		{
			//debug_print("Analysis : push value %d\n", (l_data_signal.clock-fct_uname_1_in_time[proc]));
			delay.barrier.fct_uname_1_durations.push_back(l_data_signal.clock-fct_uname_1_in_time[proc]);
		}
	}
	else if (l_data_signal.pc == addresses[program_current_index].fct_uname_2_addr)
	{
		debug_print("Analysis : %X==%X => fct uname 2 (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].fct_uname_2_addr, proc);
		return_addr.tid.gp = l_data_signal.r28;
		return_addr.tid.fp = l_data_signal.r29;
		return_addr.data = l_data_signal.r31;
		ret_fct_uname_2[return_addr.tid.fp] = return_addr;

		fct_uname_2_in_time[proc] = l_data_signal.clock;

#ifndef ABSOLUTE_UNAME_FCT
		if (wakeup_in_time_set_barrier_flags[proc])
#endif
		{
			uint8_t mask = 0x1;
			mask = mask << round_cnt[proc];
			int budy = proc ^ mask;
			budy_proc_tab[proc] = -1;
			debug_print("potential budy %d, round budy %d  my round %d\n", budy, round_cnt[budy], round_cnt[proc]);
			if ((start_echange_time[budy] != 0) && (round_cnt[budy] == round_cnt[proc]))
				budy_proc_tab[proc] = budy;
			debug_print("proc %d, round %d my budy is %d\n", proc, round_cnt[proc], budy_proc_tab[proc]);
		}
	}
	// return from the uname function 2	      
	else if ((key_exist(ret_fct_uname_2,  l_data_signal.r29)) &&  
		 (ret_fct_uname_2[l_data_signal.r29].data == l_data_signal.pc) &&
		 (ret_fct_uname_2[l_data_signal.r29].tid.gp == l_data_signal.r28))
	{
		debug_print("Analysis : %X==%X => return from fct uname 2 (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].fct_uname_2_addr, proc);
		/* clear return address entry since we are not anymore in the function */
		ret_fct_uname_2.erase(l_data_signal.r29);

#ifndef ABSOLUTE_UNAME_FCT
		if (wakeup_in_time_set_barrier_flags[proc])
#endif
		{
			delay.barrier.fct_uname_2_durations.push_back(l_data_signal.clock-fct_uname_2_in_time[proc]);
			//fct_uname_3_in_time = l_data_signal.clock;
			delay.barrier.msg_rounds_durations[round_cnt[proc]].push_back(l_data_signal.clock-last_arrival_time[index_barrier_counter[proc]]);
			if ((l_data_signal.clock-last_arrival_time[index_barrier_counter[proc]]) < 1000)
				debug_print("Analysis : warning round time %d\n", (l_data_signal.clock-last_arrival_time[index_barrier_counter[proc]]));

			if (budy_proc_tab[proc] >= 0)
			{
				debug_print("messages_exchanges_durations %d (%d - %d) round %d \n", (l_data_signal.clock-start_echange_time[proc]), l_data_signal.clock,start_echange_time[proc], round_cnt[proc]);
				if (round_cnt[proc] < 2)
					messages_exchanges_intra_durations[proc].push_back(l_data_signal.clock-start_echange_time[proc]);
				else
					messages_exchanges_inter_durations[proc].push_back(l_data_signal.clock-start_echange_time[proc]);
			}
				
			start_echange_time[proc] = 0;
				
			round_cnt[proc]++;
		}
	}
	else if (l_data_signal.pc == addresses[program_current_index].fct_uname_3_addr)
	{
		debug_print("Analysis : %X==%X => fct uname 3 (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].fct_uname_3_addr, proc);
		return_addr.tid.gp = l_data_signal.r28;
		return_addr.tid.fp = l_data_signal.r29;
		return_addr.data = l_data_signal.r31;
		ret_fct_uname_3[return_addr.tid.fp] = return_addr;

		fct_uname_3_in_time[proc] = l_data_signal.clock;
		/*
		  if (wakeup_in_time_set_barrier_flags[proc])
		  {
		  delay.barrier.fct_uname_3_durations.push_back(l_data_signal.clock-fct_uname_3_in_time);
		  }
		*/
	}
	// return from the uname function 3
	else if ((key_exist(ret_fct_uname_3,  l_data_signal.r29)) &&  
		 (ret_fct_uname_3[l_data_signal.r29].data == l_data_signal.pc) &&
		 (ret_fct_uname_3[l_data_signal.r29].tid.gp == l_data_signal.r28))
	{
		debug_print("Analysis : %X==%X => return from fct uname 3 (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].fct_uname_3_addr, proc);
		/* clear return address entry since we are not anymore in the function */
		ret_fct_uname_3.erase(l_data_signal.r29);

#ifndef ABSOLUTE_UNAME_FCT
		if (wakeup_in_time_set_barrier_flags[proc])
#endif
		{
			delay.barrier.fct_uname_3_durations.push_back(l_data_signal.clock-fct_uname_3_in_time[proc]);
		}
	}
}


#endif
