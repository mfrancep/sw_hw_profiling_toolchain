#ifndef __DEF_MPI_ANALYSE_H__
#define __DEF_MPI_ANALYSE_H__

void analyse_mpi(uint8_t proc, core_signals_t &l_data_signal, delays_t &delay);

#endif /*__DEF_MPI_ANALYSE_H__*/
