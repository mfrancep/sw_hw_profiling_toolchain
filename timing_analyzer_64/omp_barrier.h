#ifndef __DEF_OMP_BARRIER_H__
#define __DEF_OMP_BARRIER_H__

void analyse_omp_barrier(uint8_t proc, core_signals_t &l_data_signal, delays_t &delay);

#endif /*__DEF_OMP_BARRIER_H__*/
