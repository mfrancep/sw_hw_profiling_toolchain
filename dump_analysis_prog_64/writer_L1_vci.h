#ifndef __DEF_WRITER_L1_VCI_H__
#define __DEF_WRITER_L1_VCI_H__

#define DUMP_FILE_L1_VCI_NAME "dump_l1_vci_prog"

void *thread_writer_L1_vci(void *arg);

#endif /*__DEF_WRITER_L1_VCI_H__*/
