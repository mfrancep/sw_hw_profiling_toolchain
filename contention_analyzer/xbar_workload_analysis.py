import sys
import numpy as np
import collections
import os
import subprocess
import matplotlib
import matplotlib.pyplot as plt

PRINT_DEBUG = False
NB_CLUSTERS = 16

Workload = collections.namedtuple('Workload', 'total_contention one_req_pending two_req_pending three_req_pending four_req_pending')
PendingType = collections.namedtuple('PendingType', 'init_1 init_2 init_3 init_4 init_5 init_6')

def pending_number(req):
    return bin(req).count("1")

if __name__ == "__main__": 
    input_file = open("xbar_workload.txt", 'r')

    outfile = open("workload_statistics.txt", 'w')
    
    statistics = []
    one_pending_types = []
    for i in range (0, NB_CLUSTERS):
        load_stat = Workload(0,0,0,0,0)
        statistics.append(load_stat)
        pend_type = PendingType(0,0,0,0,0,0)
        one_pending_types.append(pend_type)
        
    init_clock = 0
    last_clock = 0
    while(1):
        line = input_file.readline()
        
        if (len(line) == 0):
            last_clock = clock
            if (PRINT_DEBUG):
                print "end of file addr"
            break

        if (line[0] == '#'): #comment
            continue

        fields = line.split(";");
        if (len(fields) != 3):
            print "error : line " + line + " not valid"
            continue

        clock = int(fields[0],10)
        cluster = int(fields[1],10)
        pending_req = int(fields[2],10)

        if (init_clock == 0):
            init_clock = clock

        if (cluster >= NB_CLUSTERS):
            print "error cluster ", cluster
            continue
        
        l_total_contention = statistics[cluster].total_contention
        l_one_req_pending = statistics[cluster].one_req_pending
        l_two_req_pending = statistics[cluster].two_req_pending
        l_three_req_pending = statistics[cluster].three_req_pending
        l_four_req_pending = statistics[cluster].four_req_pending
        
        l_total_contention +=1
        nb_pending = pending_number(pending_req)
        if (nb_pending == 1):
            l_one_req_pending += 1

            l_init_1 = one_pending_types[cluster].init_1
            l_init_2 = one_pending_types[cluster].init_2
            l_init_3 = one_pending_types[cluster].init_3
            l_init_4 = one_pending_types[cluster].init_4
            l_init_5 = one_pending_types[cluster].init_5
            l_init_6 = one_pending_types[cluster].init_6
            if (pending_req == 1):
                l_init_1 += 1
            elif (pending_req == 2):
                l_init_2 += 1
            elif (pending_req == 4):
                l_init_3 += 1
            elif (pending_req == 8):
                l_init_4 += 1
            elif (pending_req == 16):
                l_init_5 += 1
            elif (pending_req == 32):
                l_init_6 += 1
            pend_type = PendingType(l_init_1,l_init_2,l_init_3,l_init_4,l_init_5, l_init_6)
            one_pending_types[cluster] = pend_type
        elif (nb_pending == 2):
            l_two_req_pending += 1
        elif (nb_pending == 3):
            l_three_req_pending += 1
        elif (nb_pending == 4):
            l_four_req_pending += 1

        l_stat = Workload(l_total_contention,l_one_req_pending,l_two_req_pending,l_three_req_pending,l_four_req_pending)
        statistics[cluster] = l_stat
            
    print "init_clock = ",  init_clock
    print "last_clock = ",  last_clock
    total_clock = last_clock - init_clock
    outfile.write("#total clock;" + str(total_clock)+"\n")
    outfile.write("#cluster;nb_contention;one_req_pending;two_req_pending;three_req_pending;four_req_pending;")
    outfile.write("pending_ratio;one_req_ratio;two_req_ratio;three_req_ratio;four_req_ratio\n")
    print "#core;nb_contention;one_req_pending;two_req_pending;three_req_pending;four_req_pending"
    for i in range (0, NB_CLUSTERS):
        total_ratio = (statistics[i].total_contention*100.0) / total_clock
        one_ratio = (statistics[i].one_req_pending*100.0) / statistics[i].total_contention
        two_ratio = (statistics[i].two_req_pending*100.0) / statistics[i].total_contention
        three_ratio = (statistics[i].three_req_pending*100.0) / statistics[i].total_contention
        four_ratio = (statistics[i].four_req_pending*100.0) / statistics[i].total_contention
        line = str(i) + ";" + str(statistics[i].total_contention) + ";" +  str(statistics[i].one_req_pending) + ";" + str(statistics[i].two_req_pending) + ";"
        line += str(statistics[i].three_req_pending) + ";" + str(statistics[i].four_req_pending) + ";"
        line += str(total_ratio) + ";" + str(one_ratio) + ";" + str(two_ratio) + ";"  + str(three_ratio) + ";" + str(four_ratio) +"\n"
        outfile.write(line)
        print line

    outfile.write("\n\n#one pending request details\n")
    outfile.write("#cluster;init_1;init_2;init_3;init_4;init_5;init_6\n")
    for i in range (0, NB_CLUSTERS):
        line = str(i) + ";" + str(one_pending_types[i].init_1) + ";" + str(one_pending_types[i].init_2) + ";"  + str(one_pending_types[i].init_3) + ";"  
        line += str(one_pending_types[i].init_4) + ";"  + str(one_pending_types[i].init_5) + ";"  + str(one_pending_types[i].init_6) + "\n" 
        outfile.write(line)
        print line
