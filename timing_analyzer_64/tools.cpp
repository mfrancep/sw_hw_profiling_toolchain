#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <errno.h>    //for error handling

#include "structs.h"


bool key_exist(std::map<uint32_t, signed_data_t> m, uint32_t key)
{
	if (m.find(key) == m.end() ) {
		// not found
		return false;
	}
	// found
	return true;
}

bool key_exist_uint32(std::map<uint32_t, uint32_t> m, uint32_t key)
{
	if (m.find(key) == m.end() ) {
		// not found
		return false;
	}
	// found
	return true;
}

bool key_exist_file(std::map<uint32_t, std::ofstream*> m, uint32_t key)
{
	if (m.find(key) == m.end() ) {
		// not found
		return false;
	}
	// found
	return true;
}

bool key_exist_vector_item(std::map<uint32_t, std::vector<signed_data_t> > m, uint32_t key)
{	
	if (m.find(key) == m.end() ) {
		// not found
		return false;
	}
	// found
	return true;
}

bool key_exist_lock(std::map<uint32_t, locks_info_t> m, uint32_t key)
{
	if (m.find(key) == m.end() ) {
		// not found
		return false;
	}
	// found
	return true;
}

bool signed_item_exist_in_vector(std::vector<signed_data_t> v, signed_data_t item)
{
	for (std::vector<signed_data_t>::iterator it = v.begin() ; it != v.end(); ++it)
	{
		if ((it->tid.gp == item.tid.gp) && (it->tid.fp == item.tid.fp) && (it->data == item.data))
			return true;
	}
	return false;
}

std::vector<std::string> split(const std::string &s, char delim) {
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> tokens;
	while (getline(ss, item, delim)) {
		tokens.push_back(item);
	}
	return tokens;
}

void write_debug(uint32_t thread_id, char * str)
{
	/*
	std::ofstream myfile;
	char file_name[256];
	sprintf(file_name, "%u.txt", thread_id);
	myfile.open (file_name, std::ios_base::app);;
	myfile << str;
	myfile.close();
	*/
}
