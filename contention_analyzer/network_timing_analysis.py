import sys
import numpy as np
from collections import namedtuple
import os
import subprocess
import matplotlib
import matplotlib.pyplot as plt

NB_PROCS = 16
PRINT_DEBUG = False
MAX_DELAY = 2000

# search the line matching with args in the L2 vcii file
# return the time, 0 if no line is matching
def find_L2_vci_line(ref_time, ref_addr_phy_addr, ref_srcid, ref_trdid, ref_pktid):
    L2_vci_file = open("L2_vci_file.txt", 'r')
    while(1):
        line = L2_vci_file.readline()

        if (len(line) == 0):
            if (PRINT_DEBUG):
                print "end of file addr"
            break

        if (line[0] == '#'): #comment
            continue

        fields = line.split(";");
        if (len(fields) != 8):
            print "error : vci line " + line + " not valid"
            continue

        L2_vci_time = long(fields[0],10)
        L2_vci_cluster = int(fields[1],10)
        L2_vci_addr_phy_addr = int(fields[3],10)
        L2_vci_srcid = int(fields[5],10)
        L2_vci_trdid = int(fields[6],10)
        L2_vci_pktid = int(fields[7],10)

        if (L2_vci_time < ref_time) :
            continue

        if (L2_vci_time > ref_time+MAX_DELAY):
            return 0

        if ((L2_vci_addr_phy_addr == ref_addr_phy_addr) and (L2_vci_srcid == ref_srcid) and (L2_vci_trdid == ref_trdid) and (L2_vci_pktid == ref_pktid)):
            return L2_vci_time

    return 0

# search the line matching with args in the dspin2vci file
# return the time, 0 if no line is matching
def find_dspin2vci_line(ref_time, ref_addr_phy_addr, ref_srcid, ref_trdid, ref_pktid, cmdval):
    dspin2vci_file = open("dspin2vci_file.txt", 'r')
    while(1):
        line = dspin2vci_file.readline()

        if (len(line) == 0):
            if (PRINT_DEBUG):
                print "end of file addr"
            break

        if (line[0] == '#'): #comment
            continue

        fields = line.split(";");
        if (len(fields) != 8):
            print "error : vci line " + line + " not valid"
            continue

        dspin2vci_time = long(fields[0],10)
        dspin2vci_cluster = int(fields[1],10)
        dspin2vci_addr_phy_addr = int(fields[2],10)
        dspin2vci_srcid = int(fields[3],10)
        dspin2vci_trdid = int(fields[4],10)
        dspin2vci_pktid = int(fields[5],10)
        dspin2vci_cmdval = int(fields[6],10)
        dspin2vci_cmddack = int(fields[7],10)

        if (dspin2vci_time < ref_time) :
            continue

        if (dspin2vci_time > ref_time+MAX_DELAY):
            return 0

        if ((dspin2vci_addr_phy_addr == ref_addr_phy_addr) and (dspin2vci_srcid == ref_srcid) and (dspin2vci_trdid == ref_trdid) and (dspin2vci_pktid == ref_pktid) and (dspin2vci_cmdval == cmdval)):
            return dspin2vci_time

    return 0


# search the line matching with args in the vci2dspin file
# in_cmd : 1 look for the reception of an input cmd
#               0 look for the complete emission of frame on dspin bus
# return the time, 0 if no line is matching
def find_vci2dspin_line(ref_time, ref_addr_phy_addr, ref_srcid, ref_trdid, ref_pktid, in_cmd):
    vci2dspin_file = open("vci2dspin_file.txt", 'r')
    while(1):
        line = vci2dspin_file.readline()

        if (len(line) == 0):
            if (PRINT_DEBUG):
                print "end of file addr"
            break

        if (line[0] == '#'): #comment
            continue

        fields = line.split(";");
        if (len(fields) != 8):
            print "error : vci line " + line + " not valid"
            continue

        vci2dspin_time = long(fields[0],10)
        vci2dspin_cluster = int(fields[1],10)
        vci2dspin_addr_phy_addr = int(fields[2],10)
        vci2dspin_srcid = int(fields[3],10)
        vci2dspin_trdid = int(fields[4],10)
        vci2dspin_pktid = int(fields[5],10)
        vci2dspin_cmdval = int(fields[6],10)
        vci2dspin_cmddack = int(fields[7],10)

        if (vci2dspin_time < ref_time) :
            continue

        if (vci2dspin_time > ref_time+MAX_DELAY):
            return 0

        if (vci2dspin_addr_phy_addr == ref_addr_phy_addr) and (vci2dspin_srcid == ref_srcid) and (vci2dspin_trdid == ref_trdid) and (vci2dspin_pktid == ref_pktid) and (((in_cmd == 1) and (vci2dspin_cmdval == 1)) or ((in_cmd == 0) and (vci2dspin_cmddack == 1))):
            return vci2dspin_time

    return 0
                


if __name__ == "__main__":
    addr_file = open("addr_file.txt", 'r')

    local_NIC_delays = []
    NIC_send_delays = []
    DSPIN_delays = []
    DSPIN2VCI_translate_delays = []
    distant_XBAR_delays = []
    
    outfile = open("network_delays.txt", 'w')
    outfile.write("#L1_time;vci2dspin_cmd_time;vci2dspin_send_time;dspin2vci_send_time;phy_addr;srcid;trdid;pktid;delay_L1_to_NIC;delay_send_NIC;delay_NIC_to_NIC;DSPIN2VCI_translate_delay;distant_Xbar_delay\n")
    
    while(1):
        line = addr_file.readline()
        
        if (len(line) == 0):
            if (PRINT_DEBUG):
                print "end of file addr"
            break

        if (line[0] == '#'): #comment
            continue

        fields = line.split(";");
        if (len(fields) != 9):
            print "error : vci line " + line + " not valid"
            continue

        addr_time = long(fields[0],10)
        addr_phy_addr = int(fields[5],10)
        addr_srcid = int(fields[6],10)
        addr_trdid = int(fields[7],10)
        addr_pktid = int(fields[8],10)

        start_emit_vci2dspin_time = find_vci2dspin_line(addr_time, addr_phy_addr, addr_srcid, addr_trdid, addr_pktid, 1);
        if (start_emit_vci2dspin_time == 0) :
            #print "start_emit_vci2dspin_time does not find for line addr ", line
            continue
        local_NIC_delays.append(start_emit_vci2dspin_time-addr_time);

        end_emit_vci2dspin_time = find_vci2dspin_line(start_emit_vci2dspin_time, addr_phy_addr, addr_srcid, addr_trdid, addr_pktid, 0);
        if (end_emit_vci2dspin_time == 0) :
            print "end_emit_vci2dspin_time does not find for line addr ", line
            print "delay_L1_to_NIC = ", str(start_emit_vci2dspin_time-addr_time)
            continue
        NIC_send_delays.append(end_emit_vci2dspin_time-start_emit_vci2dspin_time)

        receive_dspin2vci_time = find_dspin2vci_line(end_emit_vci2dspin_time, addr_phy_addr, addr_srcid, addr_trdid, addr_pktid, 0);
        if (receive_dspin2vci_time == 0) :
            print "receive_dspin2vci_time does not find for line addr ", line
            print "delay_L1_to_NIC = ", str(start_emit_vci2dspin_time-addr_time)
            print "delay_send_NIC = ", str(end_emit_vci2dspin_time-start_emit_vci2dspin_time) 
            continue
        DSPIN_delays.append(receive_dspin2vci_time-end_emit_vci2dspin_time)

        compute_dspin2vci_time = find_dspin2vci_line(end_emit_vci2dspin_time, addr_phy_addr, addr_srcid, addr_trdid, addr_pktid, 1);
        if (receive_dspin2vci_time == 0) :
            print "receive_dspin2vci_time does not find for line addr ", line
            print "delay_L1_to_NIC = ", str(start_emit_vci2dspin_time-addr_time)
            print "delay_send_NIC = ", str(end_emit_vci2dspin_time-start_emit_vci2dspin_time)
            print "DSPIN_delay_NIC = ", str(receive_dspin2vci_time-end_emit_vci2dspin_time)
            continue
        DSPIN2VCI_translate_delays.append(compute_dspin2vci_time-receive_dspin2vci_time)

        receive_L2_vci_time = find_L2_vci_line(receive_dspin2vci_time, addr_phy_addr, addr_srcid, addr_trdid, addr_pktid);
        if (receive_L2_vci_time == 0) :
            print "receive_dspin2vci_time does not find for line addr ", line
            print "delay_L1_to_NIC = ", str(start_emit_vci2dspin_time-addr_time)
            print "delay_send_NIC = ", str(end_emit_vci2dspin_time-start_emit_vci2dspin_time)
            print "DSPIN_delay_NIC = ", str(receive_dspin2vci_time-end_emit_vci2dspin_time)
            print "DSPIN2VCI_translate_delay = ", str(receive_dspin2vci_time-end_emit_vci2dspin_time)
            continue
        distant_XBAR_delays.append(receive_L2_vci_time-compute_dspin2vci_time) 
        
        out_str =   str(addr_time) + ";"
        out_str += str(start_emit_vci2dspin_time) + ";"
        out_str += str(end_emit_vci2dspin_time) + ";"
        out_str += str(receive_dspin2vci_time) + ";"
        out_str += str(addr_phy_addr) + ";"
        out_str += str(addr_srcid) + ";"
        out_str += str(addr_trdid) + ";"
        out_str += str(addr_pktid) + ";"
        out_str += str(start_emit_vci2dspin_time-addr_time) + ";"
        out_str += str(end_emit_vci2dspin_time-start_emit_vci2dspin_time) + ";"
        out_str += str(receive_dspin2vci_time-end_emit_vci2dspin_time) + ";"
        out_str += str(compute_dspin2vci_time-receive_dspin2vci_time) + ";"
        out_str += str(receive_L2_vci_time-compute_dspin2vci_time) + "\n"
        outfile.write(out_str)

    print "local NIC delays median : ", np.median(local_NIC_delays), "(nb ech ", len(local_NIC_delays), ")"
    print "NIC send delays median : ", np.median(NIC_send_delays), "(nb ech ", len(NIC_send_delays), ")"
    print "DSPIN delays median : ", np.median(DSPIN_delays), "(nb ech ", len(DSPIN_delays), ")"
    print "DSPIN2VCI translate delays median : ", np.median(DSPIN2VCI_translate_delays), "(nb ech ", len(DSPIN2VCI_translate_delays), ")"
    print "Distant XBar delays median : ", np.median(distant_XBAR_delays), "(nb ech ", len(distant_XBAR_delays), ")"
    
    plt.figure(1)
    plt.clf()
    plt.subplot(111)
    plt.boxplot(DSPIN_delays)
    plt.grid(True)
    plt.title('DSPIN delays')
    #plt.xlabel('core', fontsize=14, color='black')
    plt.ylabel('nb cycles', fontsize=14, color='black')
    
    plt.subplots_adjust(bottom = 0.2, hspace=2)
    plt.savefig('DSPIN_delays.png')
    subprocess.Popen(["display", "DSPIN_delays.png"])

