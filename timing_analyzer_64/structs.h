#ifndef _DEF_STRUCTS_H__
#define  _DEF_STRUCTS_H__

#include <stdint.h>
#include <vector>
#include <map>

struct addresses{
	uint32_t mutex_lock_addr;
	uint32_t mutex_unlock_addr;
	uint32_t lock_wait_addr;
	uint32_t lock_wait_priv_addr;
	uint32_t barrier_addr;
	uint32_t barrier_wait_end_addr;
	uint32_t barrier_instr_futex_wake_addr;
	uint32_t main_start_addr;
	uint32_t main_return_addr;
	uint32_t ll_instr_addr;
	uint32_t ll_instr_addr_2;
	uint32_t futex_wake_addr;
	uint32_t fct_uname_1_addr;
	uint32_t fct_uname_2_addr;
	uint32_t fct_uname_3_addr;
	uint32_t start_interest_section_addr;
	uint32_t end_interest_section_addr;
	uint32_t trig_instr_1;
	uint32_t mask_trig_instr_1;
	uint32_t trig_instr_2;
	uint32_t mask_trig_instr_2;
};

typedef struct {
	uint32_t pc;
	uint32_t r6; //a2
	uint32_t r28; //gp
	uint32_t r29; //sp
	uint32_t r31; //ra
	uint64_t clock; // clock tick associated to register value
	uint64_t clock_bis; // clock tick associated to register value
} core_signals_t;

struct lock_dyn {
	uint64_t time_req;
	uint8_t proc;
	uint32_t tid;
};

typedef struct {
	uint32_t contention;
	uint32_t reused_locking;
	uint32_t reused_lock_cluster;
	uint32_t total_locking;
	std::vector <struct lock_dyn> dyn;
} locks_info_t;

struct lock_delays{
	std::vector <uint64_t> acquire_delays;
	std::vector <uint64_t> cs_times;
	std::vector <uint64_t> go_to_sleep_delays;
	std::vector <uint64_t> contented_release_delays;
	std::vector <uint64_t> interlock_delays;
	std::vector <uint64_t> wakeup_durations;
};

struct barrier_delays{
	std::vector <uint64_t> arrival_times;
	std::vector <uint64_t> wakeup_times;
	std::vector <uint64_t> aware_delays;
	std::vector <uint64_t> aware_2_first_release_delays;
	std::vector <uint64_t> aware_2_last_release_delays;
	std::vector <uint64_t> arrival_2_last_release_delays;
	std::vector <uint64_t> wakeup_durations;
	std::vector <uint64_t> fct_uname_1_durations;
	std::vector <uint64_t> fct_uname_2_durations;
	std::vector <uint64_t> fct_uname_3_durations;
	std::vector <uint64_t> time_between_fct_uname_1;
	std::vector <std::vector <uint64_t> > msg_rounds_durations;
	std::vector <uint64_t> total_barrier_duration;
};

typedef struct {
	std::map <uint32_t, struct lock_delays> locks;
	struct barrier_delays barrier;
} delays_t;

struct thread_id {
	uint32_t gp; /* PID image */
	uint32_t fp; /* TID image */
};

typedef struct {
	struct thread_id tid;
	uint32_t data;
} signed_data_t;

typedef struct {
	uint32_t lock_id;
	uint32_t count;
} fairness_cnt_t;

#endif /* _DEF_STRUCTS_H__ */
