#ifndef _DEF_ADDR_EXTRACTION__
#define _DEF_ADDR_EXTRACTION__

#include "structs.h"

uint32_t hstring2int(std::string str);
struct addresses addr_extraction(char * file_path_exe, char * file_path_kernel);

#endif /*__ADDR_EXTRACTION__ */
