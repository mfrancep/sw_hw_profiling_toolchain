#ifndef __DEF_WRITER_L2_VCI_H__
#define __DEF_WRITER_L2_VCI_H__

#define DUMP_FILE_L2_VCI_NAME "dump_l2_vci_prog"

void *thread_writer_L2_vci(void *arg);

#endif /*__DEF_WRITER_L2_VCI_H__*/
