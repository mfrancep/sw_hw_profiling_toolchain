#ifndef __DEF_TOOLS_H__
#define __DEF_TOOLS_H__

bool key_exist(std::map<uint32_t, signed_data_t> m, uint32_t key);
bool key_exist_uint32(std::map<uint32_t, uint32_t> m, uint32_t key);
bool key_exist_vector_item(std::map<uint32_t, std::vector<signed_data_t> > m, uint32_t key);
bool key_exist_lock(std::map<uint32_t, locks_info_t> m, uint32_t key);
bool key_exist_file(std::map<uint32_t, std::ofstream*> m, uint32_t key);
bool signed_item_exist_in_vector(std::vector<signed_data_t> v, signed_data_t item);
std::vector<std::string> split(const std::string &s, char delim);
void write_debug(uint32_t thread_id, char * str);

#ifdef DEBUG
#define debug_print(args ...) printf(args)
#else
#define debug_print(args ...)
#endif

#endif /*__DEF_TOOLS_H__*/
