#include <iostream>
#include <stdio.h>

#include "structs.h"
//#include "svdpi.h"

/* shared variables */
extern volatile core_signals_t * data_signals;
extern volatile uint32_t current_clock;
extern uint8_t core_number;
/*
int dump_signals(const svBitVecVal* clock, const svBitVecVal* pc, const svBitVecVal* r6,
		 const svBitVecVal* r28, const svBitVecVal* r29, const svBitVecVal* r31, int proc_id)
{
	if (proc_id < core_number)
	{
		data_signals[proc_id].pc = (uint32_t) (*pc);
		data_signals[proc_id].r6 = (uint32_t) (*r6);
		data_signals[proc_id].r28 = (uint32_t) (*r28);
		data_signals[proc_id].r29 = (uint32_t) (*r29);
		data_signals[proc_id].r31 = (uint32_t) (*r31);

		// the last processor update the clock once all signals have been updated
		if (proc_id == (core_number-1))
			current_clock = (uint32_t) (*clock);
	}
	return 1;
}
*/
