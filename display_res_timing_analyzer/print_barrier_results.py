import sys
import os
import subprocess
import matplotlib
import numpy as np
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 8})

PRINT_IMAGE = 0
no_uname_fct2 = False
no_uname_fct3 = True

INPUT_OFFSET = 0

arrival_times = []
wakeup_times = []
aware_delays = []
aware_2_first_release_delays = []
aware_2_last_release_delays = []
aware_2_process_release_delays = []
arrival_2_last_release_delays = []
wakeup_durations = []
uname_function_1_durations = []
uname_function_2_durations = []
uname_function_3_durations = []
time_between_uname_function_1 = []
time_uname_function_1_by_process = []

if (len(sys.argv) < 3):
        print 'Usage python',  sys.argv[0], ' [path to delays file] [nb_proc]'
        sys.exit()

nb_proc = int(sys.argv[2]);
print "print ", sys.argv[1] , "with ", nb_proc, "proc"

if (len(sys.argv) > 3):
        true_nb_proc = int(sys.argv[3]);
        print "true nb proc = ", str(true_nb_proc) 
        
import csv
with open(sys.argv[1], 'rb') as csvfile:
    delaysreader = csv.reader(csvfile, delimiter=';', quotechar='|', quoting=csv.QUOTE_NONNUMERIC)
    rownum = 0
    for row in delaysreader:
        subrow = row;
        if len(subrow) > 0:
            if (type(row[0]) is str) and ("barrier delays" in row[0]):
                INPUT_OFFSET = rownum+2
            subrow.pop(0);
        if len(subrow) > 0:
            subrow.pop();
        if (INPUT_OFFSET > 0):
            for i in range(0, nb_proc):
                if rownum == INPUT_OFFSET+i:
                    arrival_times.insert(i,subrow);
                if rownum == INPUT_OFFSET+(1*nb_proc)+1+i:
                    wakeup_times.insert(i,subrow);        
                if rownum == INPUT_OFFSET+(2*nb_proc)+2+i:
                    aware_delays.insert(i,subrow);
                if rownum == INPUT_OFFSET+(3*nb_proc)+3+i:
                    aware_2_first_release_delays.insert(i,subrow);       
                if rownum == INPUT_OFFSET+(4*nb_proc)+4+i:
                    aware_2_last_release_delays.insert(i,subrow);
                if rownum == INPUT_OFFSET+(5*nb_proc)+5+i:
                    arrival_2_last_release_delays.insert(i,subrow);
                if rownum == INPUT_OFFSET+(6*nb_proc)+6+i:
                    aware_2_process_release_delays.insert(i,subrow);
                if rownum == INPUT_OFFSET+(7*nb_proc)+7+i:
                    wakeup_durations.insert(i,subrow);
                if rownum == INPUT_OFFSET+(8*nb_proc)+8+i:
                    uname_function_1_durations.insert(i,subrow);
                if rownum == INPUT_OFFSET+(9*nb_proc)+9+i:
                    uname_function_2_durations.insert(i,subrow);
                if rownum == INPUT_OFFSET+(10*nb_proc)+10+i:
                    uname_function_3_durations.insert(i,subrow);
                if rownum == INPUT_OFFSET+(11*nb_proc)+11+i:
                    time_between_uname_function_1.insert(i,subrow);
                if rownum == INPUT_OFFSET+(13*nb_proc)+13+i:
                    time_uname_function_1_by_process.insert(i,subrow);
                        
            #print ', '.join(row)
        rownum += 1

# plot results
# barrier aware delays
print "Out of scale barrier aware_delays"
MAX_RANGE = 1000
index = 0
for delays in aware_delays:
    print "proc", index, ":"
    total = 0;
    for elem in delays:
        if elem > MAX_RANGE:
            print elem, ";" 
            total += 1
    print "number of out of scale for proc", index,":", total, "\n";
    index += 1;

print aware_delays
plt.figure(1)
my_dpi=128
plt.figure(figsize=(950/my_dpi, 800/my_dpi), dpi=my_dpi)
plt.subplot(111)
plt.boxplot(aware_delays)
plt.ylim(0, MAX_RANGE)
plt.grid(True)
plt.yticks(np.arange(0, MAX_RANGE, 200.0))
plt.title('barrier aware delays by core : between the last thread arrival \n and the starting of the wakening process', fontsize=10)
plt.xlabel('core', fontsize=10, color='black')
plt.ylabel('nb cycles', fontsize=10, color='black')


plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_aware_delays.png', dpi=my_dpi*1.4)
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "barrier_aware_delays.png"])

# barrier aware_2_first_release_delays delays
print "Out of scale barrier aware_2_first_release_delays"
MAX_RANGE = 50000
index = 0
for delays in aware_2_first_release_delays:
    print "proc", index, ":"
    total = 0;
    for elem in delays:
        if elem > MAX_RANGE:
            print elem, ";" 
            total += 1
    print "number of out of scale for proc", index,":", total, "\n";
    index += 1;
    
plt.figure(2)
plt.clf()
plt.subplot(111)
plt.boxplot(aware_2_first_release_delays)
plt.ylim(0, MAX_RANGE)
plt.grid(True)
plt.title('barrier aware to first release delays by core : between the starting\n of the wakening process and the execution resuming of the first thread', fontsize=13)
plt.xlabel('core', fontsize=14, color='black')
plt.ylabel('nb cycles', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_aware_2_first_release_delays.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "barrier_aware_2_first_release_delays.png"])


# barrier aware_2_last_release_delays
print "Out of scale barrier aware_2_last_release_delays"
MAX_RANGE = 200000
index = 0
for delays in aware_2_last_release_delays:
    print "proc", index, ":"
    total = 0;
    for elem in delays:
        if elem > MAX_RANGE:
            print elem, ";" 
            total += 1
    print "number of out of scale for proc", index,":", total, "\n";
    index += 1;
    
plt.figure(3)
plt.clf()
plt.subplot(111)
plt.boxplot(aware_2_last_release_delays)
plt.ylim(0, MAX_RANGE)
plt.grid(True)
plt.title('barrier aware to last release delays by core : nb cycles between the starting \n of the wakening process and the execution resuming of the last thread')
plt.xlabel('core', fontsize=14, color='black')
plt.ylabel('nb cycles', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_aware_2_last_release_delays.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "barrier_aware_2_last_release_delays.png"])


# barrier aware_2_last_release_delays
print "Out of scale barrier arrival_2_last_release_delays"
MAX_RANGE = 200000
index = 0
for delays in arrival_2_last_release_delays:
    print "proc", index, ":"
    total = 0;
    for elem in delays:
        if elem > MAX_RANGE:
            print elem, ";" 
            total += 1
    print "number of out of scale for proc", index,":", total, "\n";
    index += 1;
    
plt.figure(4)
plt.clf()
plt.subplot(111)
plt.boxplot(arrival_2_last_release_delays)
plt.ylim(0, MAX_RANGE)
plt.grid(True)
plt.title('barrier arrival to last release delays by core : between the last \n thread arrival and the execution resuming of the last thread')
plt.xlabel('core', fontsize=14, color='black')
plt.ylabel('nb cycles', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_arrival_2_last_release_delays.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "barrier_arrival_2_last_release_delays.png"])

# barrier aware_2_last_release_delays

#for process_list in aware_2_process_release_delays:
#        for elem in process_list:
#                if elem > 100000:
#                        print "elem ", elem, " superieur a 1000000"
#                        process_list.remove(elem)

plt.figure(5)
plt.clf()
font = {'family' : 'normal', 'size'   : 13}
matplotlib.rc('font', **font)
my_dpi=128
plt.figure(figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
ax = plt.subplot(111)
ax.grid(which='both')                                                                                    
ax.grid(which='minor', alpha=0.3)                                                
ax.grid(which='major', alpha=0.8)
plt.boxplot(aware_2_process_release_delays[:true_nb_proc], 0, '', whis=[0, 100]) # don't show outlier points
plt.yscale('log')
plt.ylim(1000, 200000)
plt.grid(True)
#plt.title('Delays between barrier completion awareness and thread releases', fontsize=10)
plt.xlabel('thread wakening order', fontsize=13, color='black')
plt.ylabel('nb cycles before execution resuming', fontsize=13, color='black')
#if (nb_proc > 36):
#        major_step = [0, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60]
        #ax.set_xticks(major_step)
        #ax.set_xticks(np.arange(0, nb_proc, 1.0), minor=True)

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_aware_2_process_release_delays.png', dpi=my_dpi*1.4)
#if PRINT_IMAGE==1:
subprocess.Popen(["display", "barrier_aware_2_process_release_delays.png"])

print "average wake up time for each processes"
for i in range(0,len(aware_2_process_release_delays)):
        print "to process ", i ," avg = ", np.mean(aware_2_process_release_delays[i]), " cycles / median = ", np.median(aware_2_process_release_delays[i]), " cycles"
    
plt.figure(6)
plt.clf()
plt.subplot(111)
for proc in range(0, nb_proc):
    arrival_proc = [];
    for i in range(0, len(arrival_times[proc])):
        arrival_proc.append(proc)

    plt.plot(arrival_times[proc], arrival_proc, '+');

plt.title('barrier arrival patern')
plt.ylim(-0.5, nb_proc+0.5)
plt.legend(prop={'size': 7}, ncol=2)
plt.xlabel('cycles', fontsize=14, color='black')
plt.ylabel('core', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_arrival_patern.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "barrier_arrival_patern.png"])

plt.figure(7)
plt.clf()
plt.subplot(111)
for proc in range(0, nb_proc):
    wakeup_proc = [];
    for i in range(0, len(wakeup_times[proc])):
        wakeup_proc.append(proc)

    plt.plot(wakeup_times[proc], wakeup_proc, '+');
    
plt.title('barrier wakeup patern')
plt.ylim(-0.5, nb_proc+0.5)
plt.legend(prop={'size': 7}, ncol=2)
plt.xlabel('cycles', fontsize=14, color='black')
plt.ylabel('core', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_wakeup_patern.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "barrier_wakeup_patern.png"])

#risque contention
last_arrival_time = 0
CONTENTION_THRESHOLD = 500
contention_intervals = []
print "contention alert : "
for proc in range(0, nb_proc):
    for elem in arrival_times[proc]:
        for proc2 in range(0, nb_proc):
            for comp in arrival_times[proc2]:
                if (-CONTENTION_THRESHOLD < elem - comp < CONTENTION_THRESHOLD) and ((elem-comp) != 0):
                        #if (elem-comp) not in contention_intervals: #the reverse value not already add
                        if (-CONTENTION_THRESHOLD/10 < elem - comp < CONTENTION_THRESHOLD/10) and ((elem-comp) != 0):
                                print elem, "( ", (comp-elem), " / comp : ", comp, " );"
                        contention_intervals.append(comp-elem);
                if (comp-elem) > CONTENTION_THRESHOLD:
                        break
        arrival_times[proc].remove(elem)

print "len(contention_intervals) = ", str(len(contention_intervals))

print "\n\n\n"
low = 0
for j in [20,40,60,80,100,120,140,160,180,200,220,240,260,280,300,320,340,360,380,400]:
        string = str(low) + "-" + str(j) + ";"
        sys.stdout.write(string)
        sys.stdout.flush()
        count = 0
        for elem in contention_intervals:
                if low < elem < j:
                        count += 1
                        #print elem, ";"
        string = str(count)+";"
        sys.stdout.write(string)
        sys.stdout.flush()
        low = j
        sys.stdout.write('\n')
        sys.stdout.flush()

print "\n\n\n"
                                
plt.figure(8)
plt.clf()
ax = plt.subplot(111)
major_step = [0, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500]
minor_step = [0, 10, 20, 30, 40, 50, 75, 100, 150, 200, 250, 300, 350, 400, 450, 500]
ax.set_xticks(major_step)
ax.set_xticks(minor_step, minor=True)
ax.grid(which='both')                                                                                    
ax.grid(which='minor', alpha=0.5)                                                
ax.grid(which='major', alpha=0.5)   
plt.hist(contention_intervals, bins=minor_step, histtype='bar', lw=4, facecolor="None")
plt.grid(True)
#plt.title('Contention probability')
plt.xlabel('Cycles between two arrivals', fontsize=14, color='black')
plt.ylabel('Number of elements', fontsize=14, color='black')
plt.xlim(0, CONTENTION_THRESHOLD)

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_contention_risk.png')
#if PRINT_IMAGE==1:
subprocess.Popen(["display", "barrier_contention_risk.png"])
    
# barrier kernel wakeup durations
plt.figure(9)
plt.clf()
plt.subplot(111)
plt.boxplot(wakeup_durations)
#plt.ylim(0, MAX_RANGE)
plt.yscale('log')
plt.grid(True)
plt.title('barrier wakeup kernel function durations')
plt.xlabel('core', fontsize=14, color='black')
plt.ylabel('nb cycles spent in the function', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_wakeup_durations.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "barrier_wakeup_durations.png"])


plt.figure(10)
plt.clf()
plt.subplot(111)
plt.boxplot(uname_function_1_durations)
plt.yscale('log')
plt.grid(True)
plt.title('barrier ipi_send_msg function durations')
plt.xlabel('core', fontsize=14, color='black')
plt.ylabel('nb cycles spent in the function', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_uname_fct1_durations.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "barrier_uname_fct1_durations.png"])


full_uname_fct1_delays = []
for elem in uname_function_1_durations:
        full_uname_fct1_delays.extend(elem)

print full_uname_fct1_delays
print "len = ", len(full_uname_fct1_delays) 
print "median uname fct1 delays : ", np.median(full_uname_fct1_delays)

if (no_uname_fct2 == False) :
        plt.figure(11)
        plt.clf()
        plt.subplot(111)
        plt.boxplot(uname_function_2_durations)
        plt.yscale('log')
        plt.grid(True)
        plt.title('barrier wake_up_process function durations')
        plt.xlabel('core', fontsize=14, color='black')
        plt.ylabel('nb cycles spent in the function', fontsize=14, color='black')
        
        plt.subplots_adjust(bottom = 0.2, hspace=2)
        plt.savefig('barrier_uname_fct2_durations.png')
        if PRINT_IMAGE==1:
                subprocess.Popen(["display", "barrier_uname_fct2_durations.png"])

if (no_uname_fct3 == False) :
        plt.figure(12)
        plt.clf()
        plt.subplot(111)
        plt.boxplot(uname_function_3_durations)
        plt.yscale('log')
        plt.grid(True)
        plt.title('barrier wakeup mark_wake_futex durations')
        plt.xlabel('core', fontsize=14, color='black')
        plt.ylabel('nb cycles spent in the function', fontsize=14, color='black')
        
        plt.subplots_adjust(bottom = 0.2, hspace=2)
        plt.savefig('barrier_uname_fct3_durations.png')
        if PRINT_IMAGE==1:
                subprocess.Popen(["display", "barrier_uname_fct3_durations.png"])


plt.figure(13)
plt.clf()
plt.subplot(111)
for proc in range(0, nb_proc):
    time2_proc = [];
    for i in range(0, len(arrival_times[proc])):
        time2_proc.append(proc)

    plt.plot(arrival_times[proc], time2_proc, '+');

plt.title('barrier uname fct 2 time patern')
plt.ylim(-0.5, nb_proc+0.5)
plt.legend(prop={'size': 7}, ncol=2)
plt.xlabel('cycles', fontsize=14, color='black')
plt.ylabel('core', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_uname_fct2_patern.png')
#if PRINT_IMAGE==1:
subprocess.Popen(["display", "barrier_uname_fct2_patern.png"])

time2_diff = []
for proc in range(0, nb_proc):
        time2 = [];
        for i in range(0, len(arrival_times[proc])):
                if (proc+1 < nb_proc) and (i < len(arrival_times[proc+1])):
                        time2_diff.append(arrival_times[proc][i])
                        #print arrival_times[proc+1][i], " - ", arrival_times[proc][i], " = ", (arrival_times[proc+1][i]-arrival_times[proc][i])
                        #time2.append(arrival_times[proc+1][i]-arrival_times[proc][i])
        #time2_diff.append(time2)


np.sort(time2_diff)
print time2_diff

nb_diff = 0
nb_entre_diff = 0
for i in range(0, len(time2_diff)-1):
        diff = time2_diff[i+1]-time2_diff[i]
        if (diff > 10000):
                nb_diff +=1
                print nb_diff, "diff = ", diff, "(", time2_diff[i+1], " - ", time2_diff[i], ") (entre diff = ", nb_entre_diff, ")"
                nb_entre_diff = 0
        else:
                nb_entre_diff += 1
                
print time_between_uname_function_1
plt.figure(15)
plt.clf()
ax = plt.subplot(111)
ax.grid(which='both')                                                                                    
ax.grid(which='minor', alpha=0.3)                                                
ax.grid(which='major', alpha=0.8)
plt.boxplot(time_between_uname_function_1)
plt.yscale('log')
plt.grid(True)
plt.title('time between 2 calls of ipi_send_msg')
plt.xlabel('core', fontsize=14, color='black')
plt.ylabel('nb cycles', fontsize=14, color='black')

plt.subplots_adjust(bottom = 0.2, hspace=2)
plt.savefig('barrier_between_uname_fct1_time.png')
if PRINT_IMAGE==1:
    subprocess.Popen(["display", "barrier_between_uname_fct1_time.png"])



