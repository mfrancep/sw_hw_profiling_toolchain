import sys
from collections import namedtuple

INTERRUPT_IRQ_NAME = "vci_xicu_handle_irq"

dataLine = namedtuple('dataLine', ['indent', 'fct_name', 'duration'])

def write_line(text, color, indentation, duration):
    html_str = "<font size=\"2\" style=\"color:"+color+";\">"
    for i in range (0,indentation):
        html_str += "&nbsp;&nbsp;&nbsp;"
    text = text.replace("<", "")
    text = text.replace(">", "")
    html_str += text
    html_str += "</font>"
    html_str += "<font size=\"2\" style=\"color:black;\">"
    if (len(duration) > 0):
        html_str += " = "
    html_str += duration
    html_str += "</font><br />\n"
    return html_str

def create_tree(ref_file):
    ref_tree = []
    while(1):
        ref_line = ref_file.readline()

        if (len(ref_line) == 0):
            break        
        
        ref_fields = ref_line.split(";")
        if (len(ref_fields) != 3):
            print "error : line " + ref_line + " not valid"
            continue

        element = dataLine(ref_fields[0], ref_fields[1], ref_fields[2])
        ref_tree.append(element)

    return ref_tree



if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "usage : " + str(sys.argv[0]) + " [flat tree file 1] [flat tree file 2]"
        sys.exit(0)
    file1_name = str(sys.argv[1])
    file2_name = str(sys.argv[2])

    fd_file1 = open(file1_name, 'r')
    fd_file2 = open(file2_name, 'r')
    output_file = open("compared_files.html", 'w')

    file1_tree = []
    file2_tree = []

    file1_tree = create_tree(fd_file1)
    fd_file1.seek(0)
    fd_file2.seek(0)
    file2_tree = create_tree(fd_file2)


     #write output html header
    output_file.write("<body>\n")
    output_file.write("<div style=\"float:left;\">\n")
    output_file.write("<font size=\"3\" style=\"color:black;\">"+file1_name +"</font><br />\n")

    # search max nom of elements
    nb_max_elems = len(file2_tree)
    if len(file1_tree) > nb_max_elems:
        nb_min_elems = len(file1_tree)

    right_tree_str = ""
    index1 = 0
    index2 = 0
    while ((index1 < len(file1_tree)) and (index2 < len(file2_tree))):
        #analyse trees
        elem1 = file1_tree[index1]
        elem2 = file2_tree[index2]

        #skip of the interruption function in the comparison
        if (INTERRUPT_IRQ_NAME in elem1.fct_name):
            ind = index1
            while (file1_tree[ind].indent < elem1.indent):
                ind += 1
                if (ind >= len(file1_tree)):
                    ind = -1
                    break
                output_file.write(write_line(file1_tree[ind].fct_name, "gray", int(file1_tree[ind].indent), file1_tree[ind].duration))
                
            index1 = ind
            if (index1 == -1):
                break #end of tree => exit the loop
            elem1 = file1_tree[index1]

        if (INTERRUPT_IRQ_NAME in elem2.fct_name):
            ind = index2
            while (file2_tree[ind].indent < elem2.indent):
                ind += 1
                if (ind >= len(file2_tree)):
                    ind = -1
                    break
                right_tree_str += write_line(file2_tree[ind].fct_name, "gray", int(file2_tree[ind].indent), file2_tree[ind].duration)
                
            index2 = ind
            if (index2 == -1):
                break #end of tree => exit the loop
            elem2 = file2_tree[index2]
        
        if (elem1.fct_name in elem2.fct_name):
            # function call are identical
            # write calls in green
            output_file.write(write_line(elem1.fct_name, "green", int(elem1.indent), elem1.duration))
            right_tree_str += write_line(elem2.fct_name, "green", int(elem2.indent), elem2.duration)
        else :
            local_index1 = index1 + 1 #offset of the elem2 in tree1
            local_index2 = index2 + 1 #offset of the elem1 in tree 2
            #search elem1 in tree2
            while (elem1.fct_name not in file2_tree[local_index2]):
                local_index2 += 1
                if (local_index2 >= len(file2_tree)):
                    local_index2 = -1
                    break
            #search elem2 in tree1
            while (elem2.fct_name not in file1_tree[local_index1]):
                local_index1 += 1
                if (local_index1 >= len(file1_tree)):
                    local_index1 = -1
                    break
                
            if (local_index1 == -1) and (local_index2 == -1):
                #if there is no correspondance for the both calls we skip the line
                # if the correspondance is too far
                output_file.write(write_line(elem1.fct_name, "red", int(elem1.indent), elem1.duration))
                right_tree_str += write_line(elem2.fct_name, "red", int(elem2.indent), elem2.duration)
            else:
                # find the higest rank function call
                if ((local_index2 >= 0) and (elem1.indent < elem2.indent)):
                    # elem1 call higher than elem2 call
                    # we gave the priority to elem1 exept if elem1 has not correspondance in tree2
                    for i in range(index2, local_index2):
                        output_file.write(write_line("-", "black", int(elem2.indent), ""))
                        right_tree_str += write_line(file2_tree[i].fct_name, "black", int(file2_tree[i].indent), file2_tree[i].duration)
                    index2 = local_index2
                    if (elem1.fct_name in file2_tree[index2].fct_name): 
                        output_file.write(write_line(elem1.fct_name, "green", int(elem1.indent), elem1.duration))
                        right_tree_str += write_line(file2_tree[index2].fct_name, "green", int(file2_tree[index2].indent), file2_tree[index2].duration)
                    else:
                        output_file.write(write_line(elem1.fct_name, "red", int(elem1.indent), elem1.duration))
                        right_tree_str += write_line(file2_tree[index2].fct_name, "red", int(file2_tree[index2].indent), file2_tree[index2].duration)
                elif ((local_index1 >= 0) and (elem2.indent < elem1.indent)):
                    # elem2 call higher than elem1 call
                    # we gave the priority to elem2 exept if elem2 has not correspondance in tree1
                    for i in range(index1, local_index1):
                        right_tree_str += write_line("-", "black", int(elem1.indent), "")
                        output_file.write(write_line(file1_tree[i].fct_name, "black", int(file1_tree[i].indent), file1_tree[i].duration))
                    index1 = local_index1
                    if (elem2.fct_name in file1_tree[index1].fct_name): 
                        right_tree_str += write_line(elem2.fct_name, "green", int(elem2.indent), elem2.duration)
                        output_file.write(write_line(file1_tree[index1].fct_name, "green", int(file1_tree[index1].indent), file1_tree[index1].duration))
                    else:
                        right_tree_str += write_line(elem2.fct_name, "red", int(elem2.indent), elem2.duration)
                        output_file.write(write_line(file1_tree[index1].fct_name, "red", int(file1_tree[index1].indent), file1_tree[index1].duration))
                else:
                    if (local_index1 == -1):
                        local_index1 = len(file1_tree)-1
                    if (local_index2 == -1):
                        local_index2 = len(file2_tree)-1
                    # same level function call
                    if (local_index1 < local_index2):
                        for i in range(index1, local_index1):
                            right_tree_str += write_line("-", "black", int(elem1.indent), "")
                            output_file.write(write_line(file1_tree[i].fct_name, "black", int(file1_tree[i].indent), file1_tree[i].duration))
                        index1 = local_index1
                        if (elem2.fct_name in file1_tree[index1].fct_name): 
                            right_tree_str += write_line(elem2.fct_name, "green", int(elem2.indent), elem2.duration)
                            output_file.write(write_line(file1_tree[index1].fct_name, "green", int(file1_tree[index1].indent), file1_tree[index1].duration))
                        else:
                            right_tree_str += write_line(elem2.fct_name, "red", int(elem2.indent), elem2.duration)
                            output_file.write(write_line(file1_tree[index1].fct_name, "red", int(file1_tree[index1].indent), file1_tree[index1].duration))
                    else:
                        for i in range(index2, local_index2):
                            output_file.write(write_line("-", "black", int(elem2.indent), ""))
                            right_tree_str += write_line(file2_tree[i].fct_name, "black", int(file2_tree[i].indent), file2_tree[i].duration)
                        index2 = local_index2
                        if (elem1.fct_name in file2_tree[index2].fct_name): 
                            output_file.write(write_line(elem1.fct_name, "green", int(elem1.indent), elem1.duration))
                            right_tree_str += write_line(file2_tree[index2].fct_name, "green", int(file2_tree[index2].indent), file2_tree[index2].duration)
                        else:
                            output_file.write(write_line(elem1.fct_name, "red", int(elem1.indent), elem1.duration))
                            right_tree_str += write_line(file2_tree[index2].fct_name, "red", int(file2_tree[index2].indent), file2_tree[index2].duration)
                            
        index1 += 1
        index2 += 1

    while (index1 < len(file1_tree)):
        output_file.write(write_line(file1_tree[index1].fct_name, "red", int(file1_tree[index1].indent), file1_tree[index1].duration))
        index1 += 1

    while (index2 < len(file2_tree)):
        right_tree_str += write_line(file2_tree[index2].fct_name, "red", int(file2_tree[index2].indent), file2_tree[index2].duration)
        index2 += 1
        
    output_file.write("</div>\n")
    #write the other div
    output_file.write("<div style=\"float:left;margin-left:100px;\">\n")
    output_file.write("<font size=\"3\" style=\"color:black;\">"+file2_name +"</font><br />\n")
    output_file.write(right_tree_str)
    output_file.write("</div>\n");
    output_file.write("</body>")
    output_file.close()
            

    """
    offset1 = 0
    offset2 = 0
    for i in range (0, nb_max_elems):
        
        if ((i+offset1) < len(file1_tree)):
            elem1 = file1_tree[i + offset1]
        else:
            right_tree_str += write_line(file2_tree[i + offset2].fct_name, "black", int(file2_tree[i + offset2].indent), file2_tree[i + offset2].duration)
            output_file.write(write_line("-", "black", 0, ""))
            continue
        if ((i+offset2) < len(file2_tree)):
            elem2 = file2_tree[i + offset2]
        else:
            output_file.write(write_line(file1_tree[i + offset1].fct_name, "black", int(file1_tree[i + offset1].indent), file1_tree[i + offset1].duration))
            right_tree_str += write_line("-", "black", 0, "")
            continue

        #if ("idle_cpu" in elem1.fct_name):
        #    print "elem 1 name " + elem1.fct_name + " indent " + str(elem1.indent) + " jump " + str(elem1.jump) + " duration " + str(elem1.duration)
        #    print "elem 2 name " + elem2.fct_name + " indent " + str(elem2.indent) + " jump " + str(elem2.jump) + " duration " + str(elem2.duration)
        
        if (elem1.indent > elem2.indent) : # prio to elem1
            if (elem1.jump == -1):
                # write left red and jump a line in right
                output_file.write(write_line(elem1.fct_name, "red", int(elem1.indent), elem1.duration))
                right_tree_str += write_line("-", "red", int(elem1.indent), "")
            else:
                for j in range (0, elem1.jump):
                    elem2 = file2_tree[i + j]
                    right_tree_str += write_line(elem2.fct_name, "black", int(elem2.indent), elem2.duration)
                    output_file.write(write_line("-", "black", int(elem2.indent), ""))

                if (elem1.jump > 1):
                    offset2 += elem1.jump
                    elem2 = file2_tree[i + offset2]
                output_file.write(write_line(elem1.fct_name, "green", int(elem1.indent), elem1.duration))
                right_tree_str += write_line(elem2.fct_name, "green", int(elem2.indent), elem2.duration)
                
        elif (elem2.indent > elem1.indent) : # prio to elem2
            if (elem2.jump == -1):
                # write left red and jump a line in right
                right_tree_str += write_line(elem1.fct_name, "red", int(elem1.indent), elem1.duration)
                output_file.write(write_line("-", "red", int(elem1.indent), ""))
            else:
                for j in range (0, elem2.jump):
                    elem1 = file1_tree[i + j]
                    output_file.write(write_line(elem1.fct_name, "black", int(elem1.indent), elem1.duration))
                    right_tree_str += write_line("-", "black", int(elem1.indent), "")

                if (elem2.jump > 1):
                    print "avant offset1 " + str(offset1) 
                    offset1 += elem2.jump
                    print "offset1 " + str(offset1) + " elem 2 jump " + str(elem2.jump)
                    elem1 = file1_tree[i + offset1]
                output_file.write(write_line(elem1.fct_name, "green", int(elem1.indent), elem1.duration))
                right_tree_str += write_line(elem2.fct_name, "green", int(elem2.indent), elem2.duration)

        else:
            # the 2 functions have the same level
            if (elem2.jump == -1) or (elem1.jump <= elem2.jump): # prio to the function with the less nb of jump
                 # nb of jump equal => prio to 1
                for j in range (0, elem1.jump):
                    elem2 = file2_tree[i + j]
                    right_tree_str += write_line(elem2.fct_name, "black", int(elem2.indent), elem2.duration)
                    output_file.write(write_line("-", "black", int(elem2.indent), ""))

                if (elem1.jump > 1):
                    offset2 += elem1.jump
                    elem2 = file2_tree[i + offset2]
                output_file.write(write_line(elem1.fct_name, "green", int(elem1.indent), elem1.duration))
                right_tree_str += write_line(elem2.fct_name, "green", int(elem2.indent), elem2.duration)
            elif (elem1.jump == -1) or (elem2.jump < elem1.jump): # prio to the function with the less nb of jump
                for j in range (0, elem2.jump):
                    elem1 = file1_tree[i + j]
                    output_file.write(write_line(elem1.fct_name, "black", int(elem1.indent), elem1.duration))
                    right_tree_str += write_line("-", "black",  int(elem1.indent), "")

                if (elem2.jump > 1):
                    offset1 += elem2.jump
                    elem1 = file1_tree[i + offset1]
                output_file.write(write_line(elem1.fct_name, "green", int(elem1.indent), elem1.duration))
                right_tree_str += write_line(elem2.fct_name, "green", int(elem2.indent), elem2.duration)
"""
