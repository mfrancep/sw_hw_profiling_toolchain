import sys

if (len(sys.argv) < 2):
        print 'Usage python',  sys.argv[0], ' [path to log file]'
        sys.exit()
        
old_time = 0       
with open(sys.argv[1], "r") as f:
    for line in f.readlines():
        if line[0] == "#":
            continue
        fields = line.split(";");
        if int(fields[0]) < old_time:
            print("discontinuite line : ", line, " => old time = ", old_time)
        old_time = int(fields[0])
