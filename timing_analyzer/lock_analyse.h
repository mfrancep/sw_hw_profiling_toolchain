#ifndef __DEF_LOCK_ANALYSE_H__
#define __DEF_LOCK_ANALYSE_H__

void analyse_lock(uint8_t proc, core_signals_t &l_data_signal, delays_t &delay);

#endif /*__DEF_LOCK_ANALYSE_H__*/
