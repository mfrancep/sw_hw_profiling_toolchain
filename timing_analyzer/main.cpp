#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <csignal>
#include <fstream>
#include <string.h>
#include <sstream>

#include "structs.h"
#include "addr_extraction.h"
#include "data_analysis.h"
#include "signals_dump.h"

/* global variables */
volatile bool exit_flag = false; //ending request
uint8_t program_current_index = 0;

/* shared variables */
volatile core_signals_t * data_signals;
std::vector <struct addresses> addresses;
uint8_t core_number = 0;

void signal_interrupt_handler( int signum )
{
	std::cout << "!!!! Interrupt signal (" << signum << ") received. !!!!\n";
	// cleanup and close up stuff here  
	// terminate program
	exit_flag = true;
	printf("exit flag set to true\n");

	signal(SIGINT, signal_interrupt_handler);
	signal(SIGTERM, signal_interrupt_handler);
	signal(SIGKILL, signal_interrupt_handler);
	signal(SIGQUIT, signal_interrupt_handler);
}

uint32_t string2int(std::string str)
{
	unsigned int x;   
	std::stringstream ss;
	ss << std::dec << str;
	ss >> x;
	return x;
}

void print_address(struct addresses addr)
{
	printf("mutex_lock_addr %u / %X\n", addr.mutex_lock_addr, addr.mutex_lock_addr);
	printf("mutex_unlock_addr %u / %X\n", addr.mutex_unlock_addr, addr.mutex_unlock_addr);
	printf("lock_wait_addr %u / %X\n", addr.lock_wait_addr, addr.lock_wait_addr);
	printf("lock_wait_priv_addr %u / %X\n", addr.lock_wait_priv_addr, addr.lock_wait_priv_addr);
	printf("barrier_addr %u / %X\n", addr.barrier_addr, addr.barrier_addr);
	printf("barrier_wait_end_addr %u / %X\n", addr.barrier_wait_end_addr, addr.barrier_wait_end_addr);
	printf("barrier_instr_futex_wake_addr %u / %X\n", addr.barrier_instr_futex_wake_addr, addr.barrier_instr_futex_wake_addr);
	printf("main_start_addr %u / %X\n", addr.main_start_addr, addr.main_start_addr);
	printf("main_return_addr %u / %X\n", addr.main_return_addr, addr.main_return_addr);
	printf("ll_instr_addr %u / %X\n", addr.ll_instr_addr, addr.ll_instr_addr);
	printf("ll_instr_addr_2 %u / %X\n", addr.ll_instr_addr_2, addr.ll_instr_addr_2);
	printf("futex_wake_addr %u / %X\n", addr.futex_wake_addr, addr.futex_wake_addr);
	printf("fct_uname_1_addr %u / %X\n", addr.fct_uname_1_addr, addr.fct_uname_1_addr);
	printf("fct_uname_2_addr %u / %X\n", addr.fct_uname_2_addr, addr.fct_uname_2_addr);
	printf("fct_uname_3_addr %u / %X\n", addr.fct_uname_3_addr, addr.fct_uname_3_addr);
	printf("start_interest_section_addr %u / %X\n", addr.start_interest_section_addr, addr.start_interest_section_addr);
	printf("end_interest_section_addr %u / %X\n", addr.end_interest_section_addr, addr.end_interest_section_addr);
	printf("trig_instr_1 %u / %X\n", addr.trig_instr_1, addr.trig_instr_1);
	printf("mask_trig_instr_1 %u / %X\n", addr.mask_trig_instr_1, addr.mask_trig_instr_1);
	printf("trig_instr_2 %u / %X\n", addr.trig_instr_2, addr.trig_instr_2);
	printf("mask_trig_instr_2 %u / %X\n", addr.mask_trig_instr_2, addr.mask_trig_instr_2);
}

int read_addr_file(char * file_path)
{
	std::ifstream addrFile;
	addrFile.open (file_path);
	if (!addrFile.is_open())
	{
		fprintf(stderr, "can't open addresses files\n");
		return -1;
	}
	std::string line;
	std::string subline;
	struct addresses addr;
	bool push_flag = false;
	while(std::getline(addrFile, line))
	{
		if (line.find("#") != std::string::npos){
			subline = line.substr(line.find("#")+1);
			std::cout << "exectutable name " << subline << "\n";
			memset(&addr,0,sizeof(struct addresses));
		}
		else if (line.find("mutex_lock_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.mutex_lock_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("mutex_unlock_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.mutex_unlock_addr = string2int(subline);
			push_flag = true;
		}
			else if (line.find("lock_wait_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.lock_wait_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("lock_wait_priv_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.lock_wait_priv_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("barrier_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.barrier_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("barrier_wait_end_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.barrier_wait_end_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("barrier_instr_futex_wake_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.barrier_instr_futex_wake_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("main_start_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.main_start_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("main_return_addr") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.main_return_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("ll_instr_addr:") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.ll_instr_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("ll_instr_addr_2:") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.ll_instr_addr_2 = string2int(subline);
			push_flag = true;
		}
		else if (line.find("futex_wake") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.futex_wake_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("fct_uname_1") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.fct_uname_1_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("fct_uname_2") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.fct_uname_2_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("fct_uname_3") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.fct_uname_3_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("start_interest_section") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.start_interest_section_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("end_interest_section") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.start_interest_section_addr = string2int(subline);
			push_flag = true;
		}
		else if (line.find("trig_instr_1") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.trig_instr_1 = string2int(subline);
			push_flag = true;
		}
		else if (line.find("mask_instr_1") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.mask_trig_instr_1 = string2int(subline);
			push_flag = true;
		}
		else if (line.find("trig_instr_2") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.trig_instr_2 = string2int(subline);
			push_flag = true;
		}
		else if (line.find("mask_instr_2") != std::string::npos){
			subline = line.substr(line.find(":")+1);
			addr.mask_trig_instr_2 = string2int(subline);
			//last address, we push all in the vector
			addresses.push_back(addr);
			push_flag = false;
		}
	}
	if (push_flag)
		addresses.push_back(addr);

	/*
	for(int i =0; i<addresses.size(); i++)
	{
		printf("address %d: \n", i);
		print_address(addresses[i]);
	}
	*/
	
	addrFile.close();
	return 1;
}


int main(int argc, char *argv[])
{
      // read configuration file
      std::ifstream myfile;
      myfile.open("inputs.conf");
      if (!myfile.is_open())
	{
	  printf("file not open\n");
	  return 0;
	}
      printf("read inputs from config files\n");
      std::string line;
      while(std::getline(myfile, line))
	{
	  if (line.find("core number") != std::string::npos){
	    std::string data = line.substr(line.find("=")+1);
	    core_number = string2int(data);
	    printf("core_number %d\n", core_number); 
	  }
	}
      myfile.close();

      if (argc > 1) // dump_file index
      {
	      program_current_index = atoi(argv[1]);
      }
      
      printf("Start analysing signals for %d core exe index %d\n", core_number, program_current_index);

      if (read_addr_file("extracted_addresses.txt") < 0)
	      return 0;

	/* shared memory struct initialization */
      data_signals = new core_signals_t [core_number];
      memset((void*)data_signals, 0, core_number*sizeof(core_signals_t));
      exit_flag = false;

      signal(SIGINT, signal_interrupt_handler);
      signal(SIGTERM, signal_interrupt_handler);
      signal(SIGKILL, signal_interrupt_handler);
      signal(SIGQUIT, signal_interrupt_handler);
	
      /* run threads : analyse thread and signals dumping thread */
      pthread_t analysis_thread;
      try {
	      /* run analyse thread */
	      if(pthread_create(&analysis_thread , NULL , thread_analyse , NULL) < 0)
	      {
		      perror("could not create thread analyse");
		      return 1;
	      }
	      
		while(!exit_flag) {
			sleep(1);
		}

		//Now join the threads, so that we dont terminate before the thread
		pthread_join(analysis_thread , NULL);
		
		delete [] data_signals;
	}
	catch( std::string message ) {
		std::cerr << message << std::endl;
		std::cerr << "Fatal Error: Program aborting." << std::endl;
		delete [] data_signals;
		return -1;
	}
	catch(...) {
		std::cerr << "Error: Unclassified exception." << std::endl;
		std::cerr << "Fatal Error: Program aborting." << std::endl;
		delete [] data_signals;
		return -1;
	}
	return 0;
}
