#include <iostream>
#include <stdio.h>

#include "structs.h"
#include "svdpi.h"
#include "tbxbindings.h"

extern std::vector <struct addresses> addresses;

extern int read_addr_file(char * file_path);
extern bool addresses_loaded;
extern unsigned int interest_section_fine_step;

extern uint8_t num_exe;

int load_trig_addresses(svBitVecVal* mutex_lock_addr_out, svBitVecVal* mutex_unlock_addr_out,
			svBitVecVal* lock_wait_addr_out, svBitVecVal* lock_wait_priv_addr_out,
			svBitVecVal* barrier_addr_out, svBitVecVal* barrier_wait_end_addr_out,
			svBitVecVal* barrier_instr_futex_wake_addr_out, svBitVecVal* main_start_addr_out,
			svBitVecVal* ll_instr_addr_out, svBitVecVal* ll_instr_addr_2_out,
			svBitVecVal* futex_wake_addr, svBitVecVal* fct_uname_1_addr,
			svBitVecVal* fct_uname_2_addr, svBitVecVal* fct_uname_3_addr,
			svBitVecVal* start_interest_section_addr, svBitVecVal* end_interest_section_addr,
			svBitVecVal* return_main_addr, svBitVecVal* trig_instr_1, svBitVecVal* mask_trig_instr_1,
			svBitVecVal* trig_instr_2, svBitVecVal* mask_trig_instr_2,
			svBitVecVal* detailled_interest_section)
{
	printf("call to load_trig_addresses\n");
	if(!addresses_loaded)
	{
		// load addresses
		if (read_addr_file((char *)ADDR_FILE_PATH) < 0)
		{
			printf("error reading addr file\n");
			return  -1;
		}
		
	}
	
	*mutex_lock_addr_out = (svBitVecVal) addresses[num_exe].mutex_lock_addr;
	*mutex_unlock_addr_out = (svBitVecVal) addresses[num_exe].mutex_unlock_addr;
	*lock_wait_addr_out = (svBitVecVal) addresses[num_exe].lock_wait_addr;
	*lock_wait_priv_addr_out = (svBitVecVal) addresses[num_exe].lock_wait_priv_addr;
	*barrier_addr_out = (svBitVecVal) addresses[num_exe].barrier_addr;
	*barrier_wait_end_addr_out = (svBitVecVal) addresses[num_exe].barrier_wait_end_addr;
	*barrier_instr_futex_wake_addr_out = (svBitVecVal) addresses[num_exe].barrier_instr_futex_wake_addr;
	*ll_instr_addr_out = (svBitVecVal) addresses[num_exe].ll_instr_addr;
	*ll_instr_addr_2_out = (svBitVecVal) addresses[num_exe].ll_instr_addr_2;
	*futex_wake_addr = (svBitVecVal) addresses[num_exe].futex_wake_addr;
	*fct_uname_1_addr = (svBitVecVal) addresses[num_exe].fct_uname_1_addr;
	*fct_uname_2_addr = (svBitVecVal) addresses[num_exe].fct_uname_2_addr;
	*fct_uname_3_addr = (svBitVecVal) addresses[num_exe].fct_uname_3_addr;
	*start_interest_section_addr = (svBitVecVal) addresses[num_exe].start_interest_section_addr;
	*end_interest_section_addr = (svBitVecVal) addresses[num_exe].end_interest_section_addr;	
	*main_start_addr_out = (svBitVecVal) addresses[num_exe].main_start_addr;	
	// we want to send the return prog main address to be able to detect prog swithing
	// in order to load new addresses set
	*return_main_addr = (svBitVecVal) addresses[num_exe].main_return_addr;

	*trig_instr_1 = (svBitVecVal) addresses[num_exe].trig_instr_1;
	*mask_trig_instr_1 = (svBitVecVal) addresses[num_exe].mask_trig_instr_1;
	*trig_instr_2 = (svBitVecVal) addresses[num_exe].trig_instr_2;
	*mask_trig_instr_2 = (svBitVecVal) addresses[num_exe].mask_trig_instr_2;
	*detailled_interest_section = (svBitVecVal) interest_section_fine_step;
	printf("interest_section_fine_step %d\n", interest_section_fine_step);
		
	return 1;
}

