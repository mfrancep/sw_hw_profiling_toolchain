#include <iostream>
#include <stdio.h>

#include "structs.h"
#include "svdpi.h"
#include "tbxbindings.h"

/* shared variables */
extern volatile vci_t * L2_vci;
extern uint8_t core_number;
extern pthread_mutex_t *mutexes;

int dump_L2_vci(const svBitVecVal* vci_cmd, const svBitVecVal* phy_addr, const svBitVecVal* srcid, const svBitVecVal* trdid, const svBitVecVal* pktid, unsigned int clock, unsigned int cluster_id)
{
	if (cluster_id < (core_number/4))
	{
		pthread_mutex_lock(&mutexes[cluster_id]);
		L2_vci[cluster_id].pc = 0;
		L2_vci[cluster_id].inst = 0;
		L2_vci[cluster_id].vci_cmd = (uint32_t) (*vci_cmd);
		L2_vci[cluster_id].srcid = (uint32_t) (*srcid);
		L2_vci[cluster_id].trdid = (uint32_t) (*trdid);
		L2_vci[cluster_id].pktid = (uint32_t) (*pktid);
		L2_vci[cluster_id].phy_addr = (uint64_t)*phy_addr;
		L2_vci[cluster_id].cluster_id = cluster_id;
		L2_vci[cluster_id].clock = clock;
		pthread_mutex_unlock(&mutexes[cluster_id]);
		printf("(ADDR] proc %d => clock %d : pc %X / vci_cmd %d / phy_addr %lX \n", cluster_id, clock, L2_vci[cluster_id].pc, L2_vci[cluster_id].vci_cmd, L2_vci[cluster_id].phy_addr);
	}
	return 1;
}
