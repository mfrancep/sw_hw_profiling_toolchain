#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include <math.h>

#include <errno.h>    //for error handling

#include "structs.h"
#include "tools.h"
#include "lock_analyse.h"

//#define MPI_EVAL
#ifdef MPI_EVAL
#include "mpi_analyse.h"
#else
#include "omp_barrier.h"
#endif

#define STACK_SIZE 0x003FFFFF //(2MB)

// uncomment the following to analyse wake durations for lock mecanisms
//#define WAKE_FOR_LOCK


#define DUMP_FILE_NAME "dump_signals_prog"
extern uint8_t program_current_index;

/* shared variables */
extern volatile core_signals_t * data_signals;
extern std::vector <struct addresses> addresses;
extern uint8_t core_number;

extern volatile bool exit_flag; //ending request

extern  uint32_t string2int(std::string str);
extern  uint64_t string2long(std::string str);

/* global variables */
std::ofstream dyn_file;
std::ofstream fail_file;
/* current_locks => key : @ lock  /  data : lock info */
std::map <uint32_t, locks_info_t> locks_info;

// flag indicating the need to skeep the following instruction
bool skip_next = false;

/* index barrier omp */
uint8_t * barrier_index;

uint32_t nb_time_exceeded = 0;
uint32_t nb_time_exceeded_aware = 0;
// this vector array is used to stock all release delays between the barrier aware time and real process released.
// we assume the number of processes is always egual to or less than the core number
std::vector <uint64_t> * aware_2_process_release_delays;
uint64_t *  wakeup_in_time;
uint64_t * fct_uname_1_in_time;
uint64_t * fct_uname_2_in_time;
uint64_t * fct_uname_3_in_time;
uint64_t * last_fct_uname1_time;
std::vector <uint64_t> * fct_uname_1_durations_by_process;
std::vector <uint64_t> * fct_uname_1_to_fct_uname_2_by_process;
std::vector <uint64_t> * fct_uname_2_time_by_process;
bool * threads_running;

#ifdef MPI_EVAL
uint8_t * index_barrier_counter;
// flag to signal that we are in the aware state
uint8_t * wakeup_in_time_set_barrier_flags;
uint8_t * round_cnt;
uint8_t nb_rounds = 0;
std::vector <uint64_t> * messages_exchanges_intra_durations;
std::vector <uint64_t> * messages_exchanges_inter_durations;
uint64_t * start_echange_time;
int * budy_proc_tab;
#endif

// DEBUG
std::vector <uint64_t> sync_durations;
uint64_t sum_release_delays = 0;
uint64_t sum_barrier_time = 0;
uint64_t sum_barrier_time_without_1st = 0;
uint64_t total_application_delay = 0;

std::vector <uint64_t> * awake_durations;

int load_data(std::ifstream &dump_file, core_signals_t * data_sig, uint8_t &proc)
{
	std::string line;
	std::vector<std::string> sublines;
	uint32_t line_count = 0;
	uint64_t clock;
	if (std::getline(dump_file, line))
	{
		line_count++;
		// skip the header
		if (line.find("#") != std::string::npos)
		{
			return 0;
		}
		
		sublines = split(line, ';');
		for (unsigned int i=0; i<sublines.size(); i++)
		{
			switch(i)
			{
			case 0 : clock = string2long(sublines[i]); break;
			case 1 :
				proc = (uint8_t) string2int(sublines[i]);
				data_sig[proc].clock = clock;
				break;
			case 2 : data_sig[proc].pc = string2int(sublines[i]); break;
			case 3 : data_sig[proc].r6 = string2int(sublines[i]); break;
			case 4 : data_sig[proc].r28 = string2int(sublines[i]); break;
			case 5 : data_sig[proc].r29 = string2int(sublines[i]); break;
			case 6 : data_sig[proc].r31 = string2int(sublines[i]); break;	
			}
		}
		return 1;
	}
	else
	{
		return -1;
	}
}

void * thread_analyse(void *arg)
{
	printf("core_number %d\n", core_number);
	
	uint8_t proc, proc_curr, proc_next;
	int ret_curr=0, ret_next=0;

	delays_t * delays = new delays_t [core_number];
	std::vector<uint64_t> prog_times;
	//first prog start at 0
	prog_times.push_back(0);
	
	// barrier index init
	barrier_index = (uint8_t*) malloc(core_number*sizeof(uint8_t));
	memset(barrier_index, 1, core_number*sizeof(uint8_t));
	
	// alloc aware_2_process_release_delays 
	aware_2_process_release_delays = new std::vector <uint64_t> [core_number];
	fct_uname_1_durations_by_process = new std::vector <uint64_t> [core_number];
	fct_uname_2_time_by_process = new std::vector <uint64_t> [core_number];
	fct_uname_1_to_fct_uname_2_by_process = new std::vector <uint64_t> [core_number];
	
	//memory allocation
	wakeup_in_time = (uint64_t*) malloc(core_number*sizeof(uint64_t));
	fct_uname_1_in_time = (uint64_t*) malloc(core_number*sizeof(uint64_t));
	fct_uname_2_in_time = (uint64_t*) malloc(core_number*sizeof(uint64_t));
	fct_uname_3_in_time = (uint64_t*) malloc(core_number*sizeof(uint64_t));
	last_fct_uname1_time = (uint64_t*) malloc(core_number*sizeof(uint64_t));
	threads_running = (bool *) malloc(core_number*sizeof(bool));

	memset(wakeup_in_time, 0, core_number*sizeof(uint64_t));
	memset(fct_uname_1_in_time, 0, core_number*sizeof(uint64_t));
	memset(fct_uname_2_in_time, 0, core_number*sizeof(uint64_t));
	memset(fct_uname_3_in_time, 0, core_number*sizeof(uint64_t));
	memset(last_fct_uname1_time, 0, core_number*sizeof(uint64_t));
	memset(threads_running, true, core_number*sizeof(bool));

	
	awake_durations = new std::vector <uint64_t> [core_number];
	/*
	uint64_t old_clocks[core_number];
	memset(old_clocks, 0, core_number*sizeof(uint64_t));
	uint64_t old_pc[core_number];
	memset(old_pc, 0, core_number*sizeof(uint64_t));
	*/

#ifdef MPI_EVAL
	index_barrier_counter = (uint8_t*) malloc(core_number*sizeof(uint8_t));
	memset(index_barrier_counter, 0, core_number*sizeof(uint8_t));
	wakeup_in_time_set_barrier_flags = (uint8_t*) malloc(core_number*sizeof(uint8_t));
	memset(wakeup_in_time_set_barrier_flags, 0, core_number*sizeof(uint8_t));
	round_cnt = (uint8_t*) malloc(core_number*sizeof(uint8_t));
	memset(round_cnt, 0, core_number*sizeof(uint8_t));

	//init round vector duration
	nb_rounds = ceil(log2(core_number));
	printf("nb_round %d\n", nb_rounds);
	for(int i=0; i<ceil(log2(core_number)); i++) //TODO log2 de core_number
	{
		std::vector <uint32_t> vec;
		for(proc=0; proc<core_number; proc++)
			delays[proc].barrier.msg_rounds_durations.push_back(vec);
	}

	// messages_exchanges_duraction table allocation
	messages_exchanges_intra_durations = new std::vector <uint64_t> [core_number];
	messages_exchanges_inter_durations = new std::vector <uint64_t> [core_number];
	start_echange_time = (uint64_t*) malloc(core_number*sizeof(uint64_t));
	memset(start_echange_time, 0, core_number*sizeof(uint64_t));
	budy_proc_tab = (int*) malloc(core_number*sizeof(int));
	memset(budy_proc_tab, 0, core_number*sizeof(int));
#endif
	
	// local copies of the shared mem
	core_signals_t * l_data_signals_curr = new core_signals_t [core_number];
	core_signals_t * l_data_signals_next = new core_signals_t [core_number];
	memset(l_data_signals_curr, 0, core_number*sizeof(core_signals_t));
	memset(l_data_signals_next, 0, core_number*sizeof(core_signals_t));
	
	printf("thread analyse, core number %d\n", core_number);

	// create and open the dynamic output trace file
	dyn_file.open ("dynamic_trace.txt");
	dyn_file << "|File containing the dynamic information of locks|\n";
	dyn_file << "|time;step_code(0:enter lock function /  1:lock identification / 2:go to sleep / 3:return from lock function / 4:lock release)|;";
	dyn_file << "|proc;tid;@lock;contention;acquire delay;contented release delay;go to sleep delay;critical section duration;fairness|\n";

	// create and open the dynamic containing all failed (go to sleep) lock call 
	fail_file.open ("lock_failed_call.txt");
	
	// open the input trace file
	std::ifstream dump_file;
	char file_path[256];
	snprintf(file_path, 256, "%s%d.txt", DUMP_FILE_NAME, program_current_index);
	dump_file.open(file_path);
	printf("input dump file %s\n", file_path);

	skip_next = false;

	while(!exit_flag)
	{
		proc_curr = proc_next;
		ret_curr = ret_next;
		memcpy(l_data_signals_curr, l_data_signals_next,  core_number*sizeof(core_signals_t));
		ret_next = load_data(dump_file, l_data_signals_next, proc_next);
		
		if (ret_curr < 0) // end of file
			break;
		else if (ret_curr == 0) // header
			continue;

		/*
		// we skip the multiple cycles instruction
		if((l_data_signals_curr[proc_curr].clock == (old_clocks[proc_curr]+1)) && (l_data_signals_curr[proc_curr].pc==old_pc[proc_curr]))
		{
			old_clocks[proc_curr] = l_data_signals_curr[proc_curr].clock;
			old_pc[proc_curr] = l_data_signals_curr[proc_curr].pc;
			skip_next = false;
			continue;
		}
			
		old_clocks[proc_curr] = l_data_signals_curr[proc_curr].clock;
		old_pc[proc_curr] = l_data_signals_curr[proc_curr].pc;
		*/

		// thread stack size filter : ! we assume the thread stack size is STACK_SIZE
		l_data_signals_curr[proc_curr].r29 &= ~(STACK_SIZE);
		l_data_signals_next[proc_next].r29 &= ~(STACK_SIZE);

		if (skip_next)
		{
			printf("!skip current instruction ");
			printf("proc %d => clock %lu : pc %X(%u) / r6 %X(%u) / r28 %X(%u) / r29 %X(%u) /r31 %X(%u)\n", proc_curr, l_data_signals_curr[proc_curr].clock, 
			       l_data_signals_curr[proc_curr].pc, l_data_signals_curr[proc_curr].pc,
			       l_data_signals_curr[proc_curr].r6, l_data_signals_curr[proc_curr].r6,
			       l_data_signals_curr[proc_curr].r28, l_data_signals_curr[proc_curr].r28,
			       l_data_signals_curr[proc_curr].r29, l_data_signals_curr[proc_curr].r29, 
			       l_data_signals_curr[proc_curr].r31, l_data_signals_curr[proc_curr].r31);

			// the skip flag is not reset if the next instruction is a redondante instruction
			if ((l_data_signals_curr[proc_next].r28 == l_data_signals_next[proc_next].r28) &&
			    (l_data_signals_curr[proc_next].r29 == l_data_signals_next[proc_next].r29) &&
			    (l_data_signals_curr[proc_next].pc == l_data_signals_next[proc_next].pc))
			{
				debug_print("instruction double delete :\n");
				debug_print("first occurence : clock %lu, proc_next %d , pc %X (%u) / r28 %X(%u) / r29 %X(%u) \n",
					    l_data_signals_curr[proc_next].clock, proc_next,
					    l_data_signals_curr[proc_next].pc, l_data_signals_curr[proc_next].pc,
					    l_data_signals_curr[proc_next].r28, l_data_signals_curr[proc_next].r28,
					    l_data_signals_curr[proc_next].r29, l_data_signals_curr[proc_next].r29);
			}
			else
			{
				skip_next = false;
			}
			continue;
		}

		// we skip the first instruction in case of instruction doubling  (due to scheduler : deschedule, resechedule)
		if ((l_data_signals_curr[proc_next].r28 == l_data_signals_next[proc_next].r28) &&
		    (l_data_signals_curr[proc_next].r29 == l_data_signals_next[proc_next].r29) &&
		    (l_data_signals_curr[proc_next].pc == l_data_signals_next[proc_next].pc))
		{
			debug_print("instruction double delete :\n");
			debug_print("first occurence : clock %lu, proc_next %d , pc %X (%u) / r28 %X(%u) / r29 %X(%u) \n",
				    l_data_signals_curr[proc_next].clock, proc_next,
				    l_data_signals_curr[proc_next].pc, l_data_signals_curr[proc_next].pc,
				    l_data_signals_curr[proc_next].r28, l_data_signals_curr[proc_next].r28,
				    l_data_signals_curr[proc_next].r29, l_data_signals_curr[proc_next].r29);
			skip_next = true;							
		}
		
		printf("proc %d => clock %lu : pc %X(%u) / r6 %X(%u) / r28 %X(%u) / r29 %X(%u) /r31 %X(%u)\n", proc_curr, l_data_signals_curr[proc_curr].clock, 
		       l_data_signals_curr[proc_curr].pc, l_data_signals_curr[proc_curr].pc,
		       l_data_signals_curr[proc_curr].r6, l_data_signals_curr[proc_curr].r6,
		       l_data_signals_curr[proc_curr].r28, l_data_signals_curr[proc_curr].r28,
		       l_data_signals_curr[proc_curr].r29, l_data_signals_curr[proc_curr].r29, 
		       l_data_signals_curr[proc_curr].r31, l_data_signals_curr[proc_curr].r31);

		char str[2048];
		sprintf(str,"proc %d => clock %lu : pc %X(%u) / r6 %u / r28 %u / r29 %u /r31 %u\n", proc_curr, l_data_signals_curr[proc_curr].clock, 
		       l_data_signals_curr[proc_curr].pc, l_data_signals_curr[proc_curr].pc, l_data_signals_curr[proc_curr].r6, l_data_signals_curr[proc_curr].r28, l_data_signals_curr[proc_curr].r29, 
		       l_data_signals_curr[proc_curr].r31);
		write_debug(l_data_signals_curr[proc_curr].r29, str);

#ifdef MPI_EVAL
		l_data_signals_curr[proc_curr].r29 += proc_curr;
		debug_print("new l_data_signals_curr[proc_curr].r29 due to MPI eval : %u\n", l_data_signals_curr[proc_curr].r29);
#endif
		/***********************************************************************************************/
		/*                                                 lock analysis                                                */
		/***********************************************************************************************/
		analyse_lock(proc_curr, l_data_signals_curr[proc_curr], delays[proc_curr]);
	
		/***********************************************************************************************/
		/*                                                barrier analysis                                              */
		/***********************************************************************************************/
#ifdef MPI_EVAL
		analyse_mpi(proc_curr, l_data_signals_curr[proc_curr], delays[proc_curr]);
#else
		analyse_omp_barrier(proc_curr, l_data_signals_curr[proc_curr], delays[proc_curr]);
#endif
	}

	debug_print("\n\nnb_time_exceeded %d / nb_time_exceeded_aware %d\n", nb_time_exceeded, nb_time_exceeded_aware);
	
	// close the dynamic trace file
	dyn_file.close();

	// close fail file
	fail_file.close();


	debug_print("generate output file...\n");
	/* generate output file */
	uint32_t i;
	std::ofstream myfile;
	myfile.open ("delays.txt");
	if (myfile.fail())
	{
		std::cout << "can't create delays.txt" <<  std::endl;
	}
	myfile << "|File containing all computed delays|\n";
	myfile << "|programs times|;";
	for(i=0; i<prog_times.size();i++)
	{
		myfile << prog_times[i] << ";";
	}
	std::map<uint32_t, std::ofstream*> files;
	char file_name[128];
	for (proc=0; proc<core_number; proc++)
	{
		for (std::map<uint32_t, struct lock_delays>::iterator it=delays[proc].locks.begin(); it!=delays[proc].locks.end(); ++it)
		{
			if (key_exist_file(files, it->first))
			    continue;
			    
			sprintf(file_name, "delays_%d.txt", it->first);
			files[it->first] = new std::ofstream(file_name);
			*files[it->first] << "|File containing all computed delays|\n";
			*files[it->first] << "|programs times|;";
			for(i=0; i<prog_times.size();i++)
			{
				*files[it->first] << prog_times[i] << ";";
			}
			*files[it->first] << "\n|lock delays|\n";	
		
			*files[it->first] << "|acquire delays|";
		}
	}

	for (proc=0; proc<core_number; proc++)
	{
		for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
			*(it->second) << "\n|proc" <<(uint32_t)proc<<"|;";
		for (std::map<uint32_t, struct lock_delays>::iterator it=delays[proc].locks.begin(); it!=delays[proc].locks.end(); ++it)
		{
			for (i=0; i<it->second.acquire_delays.size();i++)
				*(files[it->first]) << it->second.acquire_delays[i] << ";";
		}
	}

	for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
		*(it->second) << "\n|critical section delays|";
	for (proc=0; proc<core_number; proc++)
	{
		for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
			*(it->second) << "\n|proc" <<(uint32_t)proc<<"|;";
		for (std::map<uint32_t, struct lock_delays>::iterator it=delays[proc].locks.begin(); it!=delays[proc].locks.end(); ++it)
		{
			for (i=0; i<it->second.cs_times.size();i++)
				*(files[it->first]) << it->second.cs_times[i] << ";";
		}
	}

	for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
		*(it->second) << "\n|go to sleep delays|";
	for (proc=0; proc<core_number; proc++)
	{
		for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
			*(it->second) << "\n|proc" <<(uint32_t)proc<<"|;";
		for (std::map<uint32_t, struct lock_delays>::iterator it=delays[proc].locks.begin(); it!=delays[proc].locks.end(); ++it)
		{
			for (i=0; i<it->second.go_to_sleep_delays.size();i++)
				*(files[it->first]) << it->second.go_to_sleep_delays[i] << ";";
		}
	}

	for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
		*(it->second) << "\n|contented release delays|";
	for (proc=0; proc<core_number; proc++)
	{
		for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
			*(it->second) << "\n|proc" <<(uint32_t)proc<<"|;";
		for (std::map<uint32_t, struct lock_delays>::iterator it=delays[proc].locks.begin(); it!=delays[proc].locks.end(); ++it)
		{
			for (i=0; i<it->second.contented_release_delays.size();i++)
				*(files[it->first]) << it->second.contented_release_delays[i] << ";";
		}
	}
	for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
		*(it->second) << "\n|interlock delays|";
	for (proc=0; proc<core_number; proc++)
	{
		for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
			*(it->second) << "\n|proc" <<(uint32_t)proc<<"|;";
		for (std::map<uint32_t, struct lock_delays>::iterator it=delays[proc].locks.begin(); it!=delays[proc].locks.end(); ++it)
		{
			for (i=0; i<it->second.interlock_delays.size();i++)
				*(files[it->first]) << it->second.interlock_delays[i] << ";";
		}
	}

	for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
		*(it->second) << "\n|wakeup durations|";
	for (proc=0; proc<core_number; proc++)
	{
		for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
			*(it->second) << "\n|proc" <<(uint32_t)proc<<"|;";
		for (std::map<uint32_t, struct lock_delays>::iterator it=delays[proc].locks.begin(); it!=delays[proc].locks.end(); ++it)
		{
			for (i=0; i<it->second.wakeup_durations.size();i++)
				*(files[it->first]) << it->second.wakeup_durations[i] << ";";
		}
	}
	
	myfile << "\n|lock informations|\n";
	myfile << "|lock contention|\n";
	for (std::map <uint32_t, locks_info_t>::iterator it=locks_info.begin(); it!=locks_info.end(); ++it)
		myfile << it->first << ";" << it->second.contention << "\n";

	myfile << "|reuse ratio (lock id; lock reused by core; lock reuse by cluster; total lock acquisition; ratio by core; ratio by cluster)|\n";
	for (std::map <uint32_t, locks_info_t>::iterator it=locks_info.begin(); it!=locks_info.end(); ++it)
		myfile << it->first << ";" << it->second.reused_locking << ";" <<  it->second.reused_lock_cluster << ";"  << it->second.total_locking << ";"
		       << int(((double)it->second.reused_locking/it->second.total_locking)*100) << ";" << int(((double)it->second.reused_lock_cluster/it->second.total_locking)*100) << "\n";
	
	myfile << "|lock dynamic ( @lock;time_1(procX/tid);time_2(procY/tid);... )|\n";
	for (std::map <uint32_t, locks_info_t>::iterator it=locks_info.begin(); it!=locks_info.end(); ++it)
	{
		myfile << it->first << ";";
		for (i=0; i<it->second.dyn.size();i++)
			myfile << "|" << it->second.dyn[i].time_req << "(" << (uint32_t)it->second.dyn[i].proc << "/" << (uint32_t)it->second.dyn[i].tid << ")|;";
		myfile << "\n";
	}
	
	myfile << "\n|barrier delays|\n";
	myfile << "|arrival times|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.arrival_times.size();i++)
			myfile << delays[proc].barrier.arrival_times[i] << ";";
	}
	myfile << "\n|wakeup times|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.wakeup_times.size();i++)
			myfile << delays[proc].barrier.wakeup_times[i] << ";";
	}
	myfile << "\n|aware delays|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.aware_delays.size();i++)
			myfile << delays[proc].barrier.aware_delays[i] << ";";
	}
	myfile << "\n|aware to first release delays|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.aware_2_first_release_delays.size();i++)
			myfile << delays[proc].barrier.aware_2_first_release_delays[i] << ";";
	}
	myfile << "\n|aware to last release delays|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.aware_2_last_release_delays.size();i++)
			myfile << delays[proc].barrier.aware_2_last_release_delays[i] << ";";
	}
	myfile << "\n|arrival to last release delays|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.arrival_2_last_release_delays.size();i++)
			myfile << delays[proc].barrier.arrival_2_last_release_delays[i] << ";";
	}
	myfile << "\n|aware to process releases delays|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|process" <<(uint32_t)proc<<"|;";
		for (i=0; i<aware_2_process_release_delays[proc].size();i++)
		{
			myfile << aware_2_process_release_delays[proc][i] << ";";
			sum_release_delays += aware_2_process_release_delays[proc][i];
		}
	}
	myfile << "\n|awake delays|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|process" <<(uint32_t)proc<<"|;";
		for (i=0; i<awake_durations[proc].size();i++)
		{
			myfile << awake_durations[proc][i] << ";";
		}
	}

	myfile << "\n|time spent in the kernel wake up function|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.wakeup_durations.size();i++)
			myfile << delays[proc].barrier.wakeup_durations[i] << ";";
	}

	unsigned long long sum_uname_fct1 = 0;
	unsigned int nb_uname_fct1 = 0; 
	myfile << "\n|time spent in the uname function 1|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.fct_uname_1_durations.size();i++)
		{
			myfile << delays[proc].barrier.fct_uname_1_durations[i] << ";";
			nb_uname_fct1++;
			sum_uname_fct1 += delays[proc].barrier.fct_uname_1_durations[i];
		}
	}

	unsigned long long sum_uname_fct2 = 0;
	unsigned int nb_uname_fct2 = 0; 
	myfile << "\n|time spent in the uname function 2|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.fct_uname_2_durations.size();i++)
		{
			myfile << delays[proc].barrier.fct_uname_2_durations[i] << ";";
			nb_uname_fct2++;
			sum_uname_fct2 += delays[proc].barrier.fct_uname_2_durations[i];
		}
	}

	myfile << "\n|time spent in the uname function 3|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.fct_uname_3_durations.size();i++)
			myfile << delays[proc].barrier.fct_uname_3_durations[i] << ";";
	}

	myfile << "\n|time between uname function 1 calls|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.time_between_fct_uname_1.size();i++)
			myfile << delays[proc].barrier.time_between_fct_uname_1[i] << ";";
	}

	myfile << "\n|fct uname 1 duration by process|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|process" <<(uint32_t)proc<<"|;";
		for (i=0; i<fct_uname_1_durations_by_process[proc].size();i++)
			myfile << fct_uname_1_durations_by_process[proc][i] << ";";
	}

	myfile << "\n|fct uname 2 time by process|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|process" <<(uint32_t)proc<<"|;";
		for (i=0; i<fct_uname_2_time_by_process[proc].size();i++)
			myfile << fct_uname_2_time_by_process[proc][i] << ";";
	}

	myfile << "\n|fct uname 1 to fct uname 2 by process|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|process" <<(uint32_t)proc<<"|;";
		for (i=0; i<fct_uname_1_to_fct_uname_2_by_process[proc].size();i++)
			myfile << fct_uname_1_to_fct_uname_2_by_process[proc][i] << ";";
	}

	myfile << "\n|total barrier duration by core|";
	sum_barrier_time = 0;
	sum_barrier_time_without_1st = 0;
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|core" <<(uint32_t)proc<<"|;";
		for (i=0; i<delays[proc].barrier.total_barrier_duration.size();i++)
		{
			myfile << delays[proc].barrier.total_barrier_duration[i] << ";";
			sum_barrier_time += delays[proc].barrier.total_barrier_duration[i];
			if (i>0)
				sum_barrier_time_without_1st += delays[proc].barrier.total_barrier_duration[i];
		}
	}

#ifdef MPI_EVAL       
	myfile << "\n|messages exhanges intra durations|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<messages_exchanges_intra_durations[proc].size();i++)
			myfile << messages_exchanges_intra_durations[proc][i] << ";";
	}

	myfile << "\n|messages exhanges inter durations|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (i=0; i<messages_exchanges_inter_durations[proc].size();i++)
			myfile << messages_exchanges_inter_durations[proc][i] << ";";
	}

	myfile << "\n|rounds durations|";
	for (proc=0; proc<core_number; proc++)
	{
		myfile << "\n|proc" <<(uint32_t)proc<<"|;";
		for (int round=0; round<nb_rounds;round++)
		{
			myfile << "\n|round" <<(uint32_t)round<<"|;";
			for (i=0; i<delays[proc].barrier.msg_rounds_durations[round].size();i++)
				myfile << delays[proc].barrier.msg_rounds_durations[round][i] << ";";
		}
	}
#endif

	debug_print("sync delays;");
	for(i=0; i<sync_durations.size(); i++)
		debug_print("%ld;", sync_durations[i]);
	debug_print("\n");

	debug_print("sum release delay : %ld\n", sum_release_delays);
	debug_print("sum barrier delay : %ld\n", sum_barrier_time);
	debug_print("sum barrier delay without 1st : %ld\n", sum_barrier_time_without_1st);
	debug_print("total appli delay : %ld\n", total_application_delay);
	
	myfile.close();

	if (nb_uname_fct1>0)
		debug_print("mean uname fct 1 = %d (%ld/%ld) \n", (sum_uname_fct1/nb_uname_fct1), sum_uname_fct1,nb_uname_fct1);
	else
		debug_print("nb_uname_fct1 = %d \n", nb_uname_fct1);
	if (nb_uname_fct2>0)
		debug_print("mean uname fct 2 = %d (%ld/%ld) \n", (sum_uname_fct2/nb_uname_fct2), sum_uname_fct2,nb_uname_fct2);
	else
		debug_print("nb_uname_fct2 = %d \n", nb_uname_fct2);

	delete [] delays;
	delete [] l_data_signals_next;
	delete [] l_data_signals_curr;
	delete [] aware_2_process_release_delays;
	delete [] fct_uname_1_durations_by_process;
	delete [] fct_uname_1_to_fct_uname_2_by_process;
	delete [] fct_uname_2_time_by_process;
	delete [] awake_durations;
	free(wakeup_in_time);
	free(fct_uname_1_in_time);
	free(fct_uname_2_in_time);	
	free(fct_uname_3_in_time);

#ifdef MPI_EVAL
	free(index_barrier_counter);
	free(wakeup_in_time_set_barrier_flags);
	free(round_cnt);
	free(start_echange_time);
#endif
	
	// Close all of the files
	for (std::map<uint32_t, std::ofstream*>::iterator it=files.begin(); it!=files.end(); ++it)
	{
		(it->second)->close();
		delete it->second;
		it->second = 0;
	}

	exit_flag = true;
	
	return NULL;
}
