#ifndef __DEF_WRITER_L2_WORKLOAD_H__
#define __DEF_WRITER_L2_WORKLOAD_H__

#define DUMP_FILE_L2_WORKLOAD_NAME "dump_l2_workload_prog"

void *thread_writer_L2_workload(void *arg);

#endif /*__DEF_WRITER_L2_WORKLOAD_H__*/
