#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>

#include <errno.h>    //for error handling

#include "structs.h"
#include "tools.h"

// comment the following line if uname function are in the barrier wakening process
//#define ABSOLUTE_UNAME_FCT

// uncomment the following if we want perform measure on bechmark mode
#define BENCHMARK_MODE

/* extern variables */
extern std::vector <struct addresses> addresses;
extern uint8_t program_current_index;
extern uint8_t core_number;
extern uint64_t nb_time_exceeded;
extern  uint64_t nb_time_exceeded_aware;
extern std::vector <uint64_t> * aware_2_process_release_delays;
extern uint64_t *  wakeup_in_time;
extern uint64_t * fct_uname_1_in_time;
extern uint64_t * fct_uname_2_in_time;
extern uint64_t * fct_uname_3_in_time;
extern uint64_t * last_fct_uname1_time;
extern uint8_t * barrier_index;
extern std::vector <uint64_t> * fct_uname_1_durations_by_process;
extern std::vector <uint64_t> * fct_uname_1_to_fct_uname_2_by_process;
extern std::vector <uint64_t> * fct_uname_2_time_by_process;
extern bool * threads_running;

/* local variables */

static signed_data_t return_addr;
/* ret_wake_futex => key : fp / data : @ return */
static std::map <uint32_t, signed_data_t> ret_wake_futex; // map used to store futex wake return addresses
/* ret_fct_uname_1 => key : fp / data : @ return */
static std::map <uint32_t, signed_data_t> ret_fct_uname_1; // map used to store uname fct 1 return addresses
/* ret_fct_uname_2 => key : fp / data : @ return */
static std::map <uint32_t, signed_data_t> ret_fct_uname_2; // map used to store uname fct 2 return addresses
/* ret_fct_uname_3 => key : fp / data : @ return */
static std::map <uint32_t, signed_data_t> ret_fct_uname_3; // map used to store uname fct 3 return addresses

// time of the last arrival of a thread/process to a barrier
static uint64_t last_arrival[2] = {0,0};
static uint64_t aware_time[2] = {0,0};
static uint64_t first_release[2] = {0,0};
static uint8_t barrier_process_number[2] = {0,0};

// flag to signal that we are in the aware state
static bool barrier_aware = false;
static bool wakeup_in_time_set_barrier_flag = false;
static bool wakeup_in_time_set_lock_flag = false;
static bool log_fct_uname1 = false;

static uint32_t total_barrier_process_nb;

static bool flag_store_uname2_duration = false;

static uint32_t fct_uname_1_num_threads = 0;
static uint32_t fct_uname_2_num_threads = 0;

#ifdef BENCHMARK_MODE
static bool benchmark_on = true; // set to false if we want test a NBP benchmark
#endif

//debug 
static uint8_t proc_aware;

static uint64_t time_sync;
extern std::vector <uint64_t> sync_durations;

extern uint64_t total_application_delay;

extern std::vector <uint64_t> * awake_durations;
static bool pending_irq = false;

void analyse_omp_barrier(uint8_t proc, core_signals_t &l_data_signal, delays_t &delay)
{
	/***********************************************************************************************/
	/*                                                barrier analysis                                              */
	/***********************************************************************************************/
#ifdef BENCHMARK_MODE
	if ((!benchmark_on) and (l_data_signal.pc != addresses[program_current_index].fct_uname_1_addr))
		return;
#endif
	
	/* we assume that only one barrier take place in a time */
	// process arrival
	if (l_data_signal.pc == addresses[program_current_index].barrier_addr)
	{
		debug_print("Analysis : %X==%X => barrier arrival (proc %d) \n", l_data_signal.pc, addresses[program_current_index].barrier_addr, proc);
		barrier_index[proc] = (barrier_index[proc]+1)%2;

		printf("l_data_signal.clock %d len %d \n", l_data_signal.clock, delay.barrier.arrival_times.size());
		delay.barrier.arrival_times.push_back(l_data_signal.clock);
		last_arrival[barrier_index[proc]] = l_data_signal.clock;
		// reinit the first release variable for the next barrier
		first_release[barrier_index[proc]] = 0;
		// count the number of processes/threads take part in the barrier
		barrier_process_number[barrier_index[proc]]++;
		debug_print("Analysis :  inc barrier_process_number[%d] %d \n", barrier_index[proc], barrier_process_number[barrier_index[proc]] );

		threads_running[proc] = false;
	}
	// master aware
	else if (l_data_signal.pc == addresses[program_current_index].barrier_instr_futex_wake_addr)
	{
		debug_print("Analysis : %X==%X => master aware time %ld (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].barrier_instr_futex_wake_addr, l_data_signal.clock, proc);
		delay.barrier.aware_delays.push_back(l_data_signal.clock-last_arrival[barrier_index[proc]]);
		aware_time[barrier_index[proc]] = l_data_signal.clock;
		barrier_aware = true;
		proc_aware = proc;
	}
	/* debug sync */
	else if (l_data_signal.pc == 0xc027707c)
	{
		time_sync = l_data_signal.clock;
	}
	else if (l_data_signal.pc == 0xc0277080)
	{
		sync_durations.push_back(l_data_signal.clock - time_sync);
	}
	/* */
	else if (l_data_signal.pc == addresses[program_current_index].barrier_wait_end_addr)
	{
		threads_running[proc] = true;
		
		debug_print("Analysis : %X==%X => release  (proc %d) (duration %ld)\n", l_data_signal.pc,  addresses[program_current_index].barrier_wait_end_addr, proc, (l_data_signal.clock-aware_time[barrier_index[proc]]));
		delay.barrier.wakeup_times.push_back(l_data_signal.clock);
		if (first_release[barrier_index[proc]] == 0)
		{
			total_barrier_process_nb = barrier_process_number[barrier_index[proc]];
			debug_print("Analysis :  first release  %ld \n", l_data_signal.clock);
			first_release[barrier_index[proc]] = l_data_signal.clock;
			delay.barrier.aware_2_first_release_delays.push_back(l_data_signal.clock-aware_time[barrier_index[proc]]);
		}

		if ((barrier_process_number[barrier_index[proc]] ==0) || (((int)total_barrier_process_nb-barrier_process_number[barrier_index[proc]]) < 0) ||
		    ((total_barrier_process_nb-barrier_process_number[barrier_index[proc]]) >= core_number))
			printf("error : time %ld process %d (%d-%d) \n", l_data_signal.clock, total_barrier_process_nb-barrier_process_number[barrier_index[proc]], total_barrier_process_nb,barrier_process_number[barrier_index[proc]]);
		else
		{
			if ((barrier_process_number[barrier_index[proc]]==1) && (l_data_signal.clock-aware_time[barrier_index[proc]]>50000))
				printf("warning : sup 50000 => %ld \n", l_data_signal.clock-aware_time[barrier_index[proc]]);
			if ((barrier_process_number[barrier_index[proc]]==2) && (l_data_signal.clock-aware_time[barrier_index[proc]]>50000))
			{
				nb_time_exceeded++;
				if(proc == proc_aware)
					nb_time_exceeded_aware++;
				printf("warning : avant dernier sup 50000 => %ld (proc aware %d)\n", l_data_signal.clock-aware_time[barrier_index[proc]], proc_aware);
			}
			if (l_data_signal.clock-aware_time[barrier_index[proc]]<1000)
				printf("warning : inf 1000 => %ld (proc aware %d)\n", l_data_signal.clock-aware_time[barrier_index[proc]], proc_aware);
			if (l_data_signal.clock < aware_time[barrier_index[proc]])
				printf("ERROROERROR : l_data_signal.clock %ld aware_time[barrier_index[proc]] %ld\n", l_data_signal.clock, aware_time[barrier_index[proc]]);
			aware_2_process_release_delays[total_barrier_process_nb-barrier_process_number[barrier_index[proc]]].push_back(l_data_signal.clock-aware_time[barrier_index[proc]]);
		}

		if (barrier_process_number[barrier_index[proc]] > 0)
			barrier_process_number[barrier_index[proc]]--;
		debug_print("Analysis : dec barrier_process_number[%d] %d \n", barrier_index[proc], barrier_process_number[barrier_index[proc]]);
		// last release process / thread
		if (barrier_process_number[barrier_index[proc]] == 0)
		{
			debug_print("Analysis : last release  %ld \n", l_data_signal.clock);
			delay.barrier.aware_2_last_release_delays.push_back(l_data_signal.clock-aware_time[barrier_index[proc]]);
			delay.barrier.arrival_2_last_release_delays.push_back(l_data_signal.clock-last_arrival[barrier_index[proc]]);
			if ((l_data_signal.clock-last_arrival[barrier_index[proc]]) < (l_data_signal.clock-aware_time[barrier_index[proc]]))
			{
				printf("error : clock %lu / aware %lu / arrival %lu => aware delay %lu / arrival delay %lu\n", l_data_signal.clock, aware_time[barrier_index[proc]], last_arrival[barrier_index[proc]], (l_data_signal.clock-aware_time[barrier_index[proc]]), (l_data_signal.clock-last_arrival[barrier_index[proc]]));
			}
			barrier_aware = false;  //reinitialization of the aware flag for the next barrier
		}
		// total barrier duration
		if (delay.barrier.arrival_times.size() > 0)
			delay.barrier.total_barrier_duration.push_back(l_data_signal.clock - delay.barrier.arrival_times.back());

		if (pending_irq)
		{
			pending_irq = false;
			awake_durations[proc].push_back(l_data_signal.clock - fct_uname_1_in_time[proc]);
		}
	}
	else if (l_data_signal.pc == addresses[program_current_index].futex_wake_addr)
	{
		debug_print("Analysis : %X==%X => futex wake (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].futex_wake_addr, proc);
		return_addr.tid.gp = l_data_signal.r28;
		return_addr.tid.fp = l_data_signal.r29;
		return_addr.data = l_data_signal.r31;
		ret_wake_futex[return_addr.tid.fp] = return_addr;

		fct_uname_1_num_threads = 0;
		fct_uname_2_num_threads = 0;
		
		if (barrier_aware)
		{
			wakeup_in_time[proc] =  l_data_signal.clock;
			wakeup_in_time_set_barrier_flag = true;
			wakeup_in_time_set_lock_flag = true;
			log_fct_uname1 = true;
			barrier_aware = false;  //reinitialization of the aware flag for the next barrier
			printf("wakeup time set to %lu\n", wakeup_in_time[proc]);
		}
	}
	// return from the futex wake function
	else if ((key_exist(ret_wake_futex,  l_data_signal.r29)) &&  
		 (ret_wake_futex[l_data_signal.r29].data == l_data_signal.pc) &&
		 (ret_wake_futex[l_data_signal.r29].tid.gp == l_data_signal.r28))
	{
		debug_print("Analysis : %X==%X => return from futex wake (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].futex_wake_addr, proc);
		/* clear return address entry since we are not anymore in the function */
		ret_wake_futex.erase(l_data_signal.r29);
		last_fct_uname1_time[proc] = 0;

#ifndef ABSOLUTE_UNAME_FCT
		if (wakeup_in_time_set_barrier_flag)
#endif
		{
			printf("add wakeup duration \n");
			delay.barrier.wakeup_durations.push_back(l_data_signal.clock-wakeup_in_time[proc]);		
			wakeup_in_time_set_barrier_flag = false;
		}
	}
	else if (l_data_signal.pc == addresses[program_current_index].fct_uname_1_addr)
	{
		debug_print("Analysis : %X==%X => fct uname 1 (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].fct_uname_1_addr, proc);
		return_addr.tid.gp = l_data_signal.r28;
		return_addr.tid.fp = l_data_signal.r29;
		return_addr.data = l_data_signal.r31;
		ret_fct_uname_1[return_addr.tid.fp] = return_addr;

		debug_print("ret_fct_uname_1[%d]\n", return_addr.tid.fp);

		//fct_uname_1_in_time[proc] = l_data_signal.clock;
		if (fct_uname_1_num_threads < core_number)
		{
			fct_uname_1_in_time[fct_uname_1_num_threads++] = l_data_signal.clock;
		}
		//flag_store_uname2_duration = true;
		
		pending_irq = true;

#ifndef ABSOLUTE_UNAME_FCT
		if (wakeup_in_time_set_barrier_flag || barrier_aware)
#endif
		{
			if (last_fct_uname1_time[proc] != 0)
			{
				delay.barrier.time_between_fct_uname_1.push_back(l_data_signal.clock-last_fct_uname1_time[proc]);
				if ((l_data_signal.clock-last_fct_uname1_time[proc]) > 100000)
					printf("WARNING : time between uname 1 %d,\n", (l_data_signal.clock-last_fct_uname1_time[proc]));
			}
		}
		
		last_fct_uname1_time[proc] = l_data_signal.clock;

#ifdef BENCHMARK_MODE
		total_application_delay = l_data_signal.clock;
		benchmark_on = true;
#endif
	}
	// return from the uname function 1
	else if ((key_exist(ret_fct_uname_1,  l_data_signal.r29)) &&  
		 (ret_fct_uname_1[l_data_signal.r29].data == l_data_signal.pc) &&
		 (ret_fct_uname_1[l_data_signal.r29].tid.gp == l_data_signal.r28))
	{
		debug_print("Analysis : %X==%X => return from fct uname 1 (proc %d) (time spent %ld flag %d )\n", l_data_signal.pc,  addresses[program_current_index].fct_uname_1_addr, proc, (l_data_signal.clock-fct_uname_1_in_time[proc]), wakeup_in_time_set_barrier_flag);
		/* clear return address entry since we are not anymore in the function */
		ret_fct_uname_1.erase(l_data_signal.r29);
		
		flag_store_uname2_duration = false;
		
#ifndef ABSOLUTE_UNAME_FCT
		if (wakeup_in_time_set_barrier_flag || barrier_aware || log_fct_uname1) 
#endif
		{
			debug_print("Analysis : fct uname 1 push value %ld\n", (l_data_signal.clock-fct_uname_1_in_time[proc]));
			delay.barrier.fct_uname_1_durations.push_back(l_data_signal.clock-fct_uname_1_in_time[proc]);
			log_fct_uname1 = false;
		}

		if (fct_uname_1_num_threads < core_number)
		{
			fct_uname_1_durations_by_process[fct_uname_1_num_threads].push_back(l_data_signal.clock-fct_uname_1_in_time[proc]);
		}
	}
	else if (l_data_signal.pc == addresses[program_current_index].fct_uname_2_addr)
	{
		debug_print("Analysis : %X==%X => fct uname 2 (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].fct_uname_2_addr, proc);
		return_addr.tid.gp = l_data_signal.r28;
		return_addr.tid.fp = l_data_signal.r29;
		return_addr.data = l_data_signal.r31;
		ret_fct_uname_2[return_addr.tid.fp] = return_addr;

		fct_uname_2_in_time[proc] = l_data_signal.clock;
		
		if (fct_uname_2_num_threads < core_number)
		{
			printf("fct_uname_2_num_threads = %d\n", fct_uname_2_num_threads);
			fct_uname_1_to_fct_uname_2_by_process[fct_uname_2_num_threads].push_back(l_data_signal.clock - fct_uname_1_in_time[fct_uname_2_num_threads]);
			fct_uname_2_time_by_process[fct_uname_2_num_threads].push_back(l_data_signal.clock);
			fct_uname_2_num_threads++;
		}
		
#ifdef BENCHMARK_MODE
		total_application_delay = l_data_signal.clock - total_application_delay;
		benchmark_on = false;
#endif
	}
	// return from the uname function 2	      
	else if (key_exist(ret_fct_uname_2,  (l_data_signal.r29)) &&  
		 (ret_fct_uname_2[l_data_signal.r29].data == l_data_signal.pc) &&
		 (ret_fct_uname_2[l_data_signal.r29].tid.gp == l_data_signal.r28))
	{
		debug_print("Analysis : %X==%X => return from fct uname 2 (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].fct_uname_2_addr, proc);
		/* clear return address entry since we are not anymore in the function */
		/*
		ret_fct_uname_2.erase(l_data_signal.r29);

#ifndef ABSOLUTE_UNAME_FCT
		if (wakeup_in_time_set_barrier_flag || flag_store_uname2_duration)
#endif
		{
			delay.barrier.fct_uname_2_durations.push_back(l_data_signal.clock-fct_uname_2_in_time[proc]);
			//fct_uname_3_in_time = l_data_signal.clock;
		}
		*/
	}
	else if (l_data_signal.pc == addresses[program_current_index].fct_uname_3_addr)
	{
		debug_print("Analysis : %X==%X => fct uname 3 (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].fct_uname_3_addr, proc);
		return_addr.tid.gp = l_data_signal.r28;
		return_addr.tid.fp = l_data_signal.r29;
		return_addr.data = l_data_signal.r31;
		ret_fct_uname_3[return_addr.tid.fp] = return_addr;

		fct_uname_3_in_time[proc] = l_data_signal.clock;
		/*
		  if (wakeup_in_time_set_barrier_flag)
		  {
		  delay.barrier.fct_uname_3_durations.push_back(l_data_signal.clock-fct_uname_3_in_time);
		  }
		*/
	}
	// return from the uname function 3
	else if ((key_exist(ret_fct_uname_3,  l_data_signal.r29)) &&  
		 (ret_fct_uname_3[l_data_signal.r29].data == l_data_signal.pc) &&
		 (ret_fct_uname_3[l_data_signal.r29].tid.gp == l_data_signal.r28))
	{
		debug_print("Analysis : %X==%X => return from fct uname 3 (proc %d) \n", l_data_signal.pc,  addresses[program_current_index].fct_uname_3_addr, proc);
		/* clear return address entry since we are not anymore in the function */
		ret_fct_uname_3.erase(l_data_signal.r29);

#ifndef ABSOLUTE_UNAME_FCT
		if (wakeup_in_time_set_barrier_flag)
#endif
		{
			delay.barrier.fct_uname_3_durations.push_back(l_data_signal.clock-fct_uname_3_in_time[proc]);
		}
	}
}
