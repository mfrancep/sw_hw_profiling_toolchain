#include <iostream>
#include <stdio.h>
#include <string.h>
#include <sstream>

#include "addr_extraction.h"

#define CMD_LEN 256
/* INSTR_SIZE and NB_INSTR_SHIFT are used to shift the start point of function in order 
   to avoid false entering in function due to pipeline invalidation */
#define INSTR_SIZE 4
#define NB_INSTR_SHIFT 0
/* ! some function change r28 global pointer value during the firsts instruction of the function
   hence, this could result in issue to trigger the retrurn address since the R28 value might be different*/

// if the following flag is not set, the programme search for GOMP barrier
#define PTHREAD_BARRIER

/* string tags to find in disassembled exe */
#define MUTEX_UNLOCK_NAME_ASM "<__pthread_mutex_unlock_internal>:\n"
#define LOCK_WAIT_NAME_ASM "<__lll_lock_wait>:\n"
#define LOCK_WAIT_PRIV_NAME_ASM "<__lll_lock_wait_private>:\n"
#ifdef PTHREAD_BARRIER
#define BARRIER_WAT_END_NAME_ASM "<pthread_barrier_wait>:\n"
#else
#define BARRIER_WAT_END_NAME_ASM "<gomp_team_barrier_wait_end>:\n"
#endif
#define MAIN_START_NAME_ASM "<main>:\n"
#define MUTEX_LOCK_NAME_ASM "<__pthread_mutex_lock_internal>:\n"
#define FUTEX_WAKE "<futex_wake>:\n"

uint32_t hstring2int(std::string str)
{
	unsigned int x;   
	std::stringstream ss;
	ss << std::hex << str;
	ss >> x;
	printf("convert %X \n", x); 
	return x;
}

std::string hex2bin(char c)
{
	switch(c)
	{
	case '0' : return "0000";
	case '1' : return "0001";
	case '2' : return "0010";
	case '3' : return "0011";
	case '4' : return "0100";
	case '5' : return "0101";
	case '6' : return "0110";
	case '7' : return "0111";
	case '8' : return "1000";
	case '9' : return "1001";
	case 'A' : case 'a' : return "1010";
	case 'B' : case 'b' : return "1011";
	case 'C' : case 'c' : return "1100";
	case 'D' : case 'd' : return "1101";
	case 'E' : case 'e' : return "1110";
	case 'F' : case 'f' : return "1111";
	}
	return NULL;
}

std::string reg_name(std::string reg)
{
	if (reg.compare("zero") == 0)
		return "R0";
	else if (reg.compare("at") == 0)
		return "R1";
	else if (reg.compare("v0") == 0)
		return "R2";
	else if (reg.compare("v1") == 0)
		return "R3";
	else if (reg.compare("a0") == 0)
		return "R4";
	else if (reg.compare("a1") == 0)
		return "R5";
	else if (reg.compare("a2") == 0)
		return "R6";
	else if (reg.compare("a3") == 0)
		return "R7";
	else if (reg.compare("t0") == 0)
		return "R8";
	else if (reg.compare("t1") == 0)
		return "R9";
	else if (reg.compare("t2") == 0)
		return "R10";
	else if (reg.compare("t3") == 0)
		return "R11";
	else if (reg.compare("t4") == 0)
		return "R12";
	else if (reg.compare("t5") == 0)
		return "R13";
	else if (reg.compare("t6") == 0)
		return "R14";
	else if (reg.compare("t7") == 0)
		return "R15";
	else if (reg.compare("s0") == 0)
		return "R16";
	else if (reg.compare("s1") == 0)
		return "R17";
	else if (reg.compare("s2") == 0)
		return "R18";
	else if (reg.compare("s3") == 0)
		return "R19";
	else if (reg.compare("s4") == 0)
		return "R20";
	else if (reg.compare("s5") == 0)
		return "R21";
	else if (reg.compare("s6") == 0)
		return "R22";
	else if (reg.compare("s7") == 0)
		return "R23";
	else if (reg.compare("t8") == 0)
		return "R24";
	else if (reg.compare("t9") == 0)
		return "R25";
	else if (reg.compare("k0") == 0)
		return "R26";
	else if (reg.compare("k1") == 0)
		return "R27";
	else if (reg.compare("gp") == 0)
		return "R28";
	else if (reg.compare("sp") == 0)
		return "R29";
	else if (reg.compare("fp") == 0)
		return "R30";
	else if (reg.compare("ra") == 0)
		return "R31";

	return "";
}

std::string get_lock_reg(std::string instruction)
{
	std::string inst = instruction.substr(instruction.find("ll"));
	std::cout << "inst " << inst << std::endl;
	std::string arg = inst.substr(inst.find("0")+2, (inst.find(")")-(inst.find("0")+2)));
	
	return reg_name(arg);
}

std::string get_addr(std::string s, bool typ_inst = false)
{
	std::string delimiter;
	if (typ_inst)
		delimiter = ":";
	else
		delimiter = " ";
	return s.substr(0, s.find(delimiter));
}

struct addresses addr_extraction(char * file_path_exe, char * file_path_kernel)
{
	FILE *fp;
	char cmd_extern[CMD_LEN];
	char buf[1024];
	std::string buf_str;
	struct addresses addr;

	/* objdump disassembled file related variables */
	int in_function_lock = 0;
        int in_function_end_barrier = 0;
	std::string lock_reg;
	bool in_main_function = false;

	/* objdump disassembling */
	snprintf(cmd_extern, CMD_LEN, "mipsel-tsar-linux-uclibc-objdump -D %s\n", file_path_exe);
	printf("try de dissable %s", cmd_extern);
	fp = popen(cmd_extern, "r");

	memset(&addr, 0, sizeof(struct addresses));
	addr.trig_instr_1 = 0xFFFFFFFF;
	addr.trig_instr_2 = 0xFFFFFFFF;
	
	/* file analysing */
	while (fgets(buf, 1024, fp)) {
		buf_str = buf;
		if (buf_str.find(MUTEX_LOCK_NAME_ASM) != std::string::npos){
			std::cout << "find lock : " << buf_str << " addr = " << get_addr(buf_str) << std::endl;
			addr.mutex_lock_addr = hstring2int(get_addr(buf_str))+(INSTR_SIZE*NB_INSTR_SHIFT);
			in_function_lock = 2; // we expect to match 2 ll instructions (one for simple mutex and another for recursive mutex)
		}else if (buf_str.find(MUTEX_UNLOCK_NAME_ASM) != std::string::npos){
			std::cout << "find unlock : " << buf_str << std::endl;
			addr.mutex_unlock_addr = hstring2int(get_addr(buf_str))+(INSTR_SIZE*NB_INSTR_SHIFT);
		}else if (buf_str.find(LOCK_WAIT_NAME_ASM) != std::string::npos){
			std::cout << "find wait : " << buf_str << std::endl;
			addr.lock_wait_addr = hstring2int(get_addr(buf_str))+(INSTR_SIZE*NB_INSTR_SHIFT);
		}else if (buf_str.find(LOCK_WAIT_PRIV_NAME_ASM) != std::string::npos){
			std::cout << "find priv wait : " << buf_str << std::endl;
			addr.lock_wait_priv_addr = hstring2int(get_addr(buf_str))+(INSTR_SIZE*NB_INSTR_SHIFT);
		}else if (buf_str.find(BARRIER_WAT_END_NAME_ASM) != std::string::npos){
			std::cout << "find barrier: " << buf_str << std::endl;
			addr.barrier_addr = hstring2int(get_addr(buf_str))+(INSTR_SIZE*NB_INSTR_SHIFT);
			in_function_end_barrier = 1;
		}else if (buf_str.find(MAIN_START_NAME_ASM) != std::string::npos){
			std::cout << "find main : " << buf_str << std::endl;
			addr.main_start_addr = hstring2int(get_addr(buf_str));
			in_main_function = true;
		}else if ((in_main_function) && (buf_str.find("jr\tra") != std::string::npos)){
			std::cout << "find main return : " << buf_str << std::endl;
			addr.main_return_addr = hstring2int(get_addr(buf_str, 1));
			in_main_function = false;
		}else if ((in_function_lock>0) && (buf_str.find("ll") != std::string::npos)){
			if (in_function_lock == 2)
				addr.ll_instr_addr = hstring2int(get_addr(buf_str, 1));
			else if (in_function_lock == 1)
				addr.ll_instr_addr_2 = hstring2int(get_addr(buf_str, 1));
			lock_reg = get_lock_reg(buf_str);
			in_function_lock--;
			std::cout << "ll asm: " << buf_str  << "addr = " << addr.ll_instr_addr  << " reg = " << lock_reg << std::endl;
#ifdef PTHREAD_BARRIER
		}else if ((in_function_end_barrier == 1) && (buf_str.find("syscall") != std::string::npos)){
#else
		}else if ((in_function_end_barrier == 1) && (buf_str.find("sync") != std::string::npos)){
#endif
			in_function_end_barrier = 2;
			addr.barrier_instr_futex_wake_addr = hstring2int(get_addr(buf_str, 1));
			std::cout << "instr futex wake (sync) in barrier end asm: " << buf_str  << "addr = " << addr.barrier_instr_futex_wake_addr << std::endl;
		}else if ((in_function_end_barrier == 2) && (buf_str.find("jr") != std::string::npos)){
			in_function_end_barrier = 0;
			addr.barrier_wait_end_addr = hstring2int(get_addr(buf_str, 1));
			std::cout << "instr jr in barrier end asm: " << buf_str  << "addr = " << addr.barrier_wait_end_addr << std::endl;
		}
	}

	fclose(fp);

	
	/* objdump disassembling kernel*/
	memset(cmd_extern, 0, CMD_LEN);
	snprintf(cmd_extern, CMD_LEN, "mipsel-unknown-elf-objdump -D %s\n", file_path_kernel);
	printf("try de dissable %s", cmd_extern);
	fp = popen(cmd_extern, "r");
	int addr_found = 0;

	/* file analysing */
	while (fgets(buf, 1024, fp)) {
		buf_str = buf;
		if (buf_str.find(FUTEX_WAKE) != std::string::npos){
			std::cout << "find futex wake : " << buf_str << std::endl;
			addr.futex_wake_addr = hstring2int(get_addr(buf_str));
			addr_found++;
		}
		if (addr_found==1)
			break;
	}

	fclose(fp);

/*
	// objdump disassembling 
	snprintf(cmd_extern, CMD_LEN, "mipsel-tsar-linux-uclibc-objdump -d %s\n", file_path);
	fp = popen(cmd_extern, "r");

	// objdump file analysing
	while (fgets(buf, 1024, fp)) {
		buf_str = buf;
		if (buf_str.find(MUTEX_LOCK_NAME_ASM) != std::string::npos){
			std::cout << "find lock asm : " << buf_str << std::endl;
			in_function_lock = true;
		}else if ((in_function_lock) && (buf_str.find("ll") != std::string::npos)){
			in_function_lock = false;
			addr.ll_instr_addr = hstring2int(get_addr(buf_str, 1));
			lock_reg = get_lock_reg(buf_str);
			std::cout << "ll asm: " << buf_str  << "addr = " << addr.ll_instr_addr  << " reg = " << lock_reg << std::endl;
		}else if (buf_str.find(BARRIER_WAT_END_NAME_ASM) != std::string::npos){
			in_function_end_barrier = true;
		}else if ((in_function_end_barrier) && (buf_str.find("<futex_wake>") != std::string::npos)){
			in_function_end_barrier = false;
			addr.barrier_instr_futex_wake_addr = hstring2int(get_addr(buf_str, 1));
			std::cout << "instr futex wake in barrier end asm: " << buf_str  << "addr = " << addr.barrier_instr_futex_wake_addr << std::endl;
		}
	}

	fclose(fp);
*/
	return addr;
}
