#include <iostream>
#include <stdio.h>

#include "structs.h"
#include "svdpi.h"
#include "tbxbindings.h"

/* shared variables */
extern volatile core_signals_t * data_signals;
extern uint8_t core_number;

extern std::vector <struct addresses> addresses;

extern bool switch_exe;
extern bool switch_exe_L1_vci;
extern bool switch_exe_L2_vci;
extern bool switch_exe_L2_workload;
extern bool switch_exe_xbar_workload;
extern uint8_t num_exe;

extern bool alert_clock;

unsigned long old_clock = 0;
extern uint32_t * old_pc;

int dump_signals(const svBitVecVal* pc, const svBitVecVal* instr, const svBitVecVal* r6, const svBitVecVal* r28,
		 const svBitVecVal* r29, const svBitVecVal* r31, const svBitVecVal* clock, const svBitVecVal* clock_bis, unsigned int proc_id)
{

	if (proc_id == 101) // switch exe
	{
		printf("switch exe\n");
		num_exe++;
		switch_exe = true;
		switch_exe_L1_vci = true;
		switch_exe_L2_vci = true;
		switch_exe_L2_workload = true;
		switch_exe_xbar_workload = true;
	}
		
  //printf("dump_signal function proc %d/%d \n", proc_id, core_number);
	if (proc_id == 130)
	{
		printf("mutex lock addr = %X\n", (uint32_t) (*pc));
		printf("mutex unlock addr = %X\n", (uint32_t) (*r6));
		printf("lock wait addr = %X\n", (uint32_t) (*r28));
		printf("lock wait priv addr = %X\n", (uint32_t) (*r29));
		printf("lock barrier addr = %X\n", (uint32_t) (*r31));
		printf("clock = %ld\n", (uint64_t) (*clock));
		printf("clock_bis = %ld\n", (uint64_t) (*clock_bis));
		alert_clock = true;
	}

	if (proc_id == 100)
	{
		printf("mutex lock addr = %X\n", (uint32_t) (*pc));
		printf("mutex unlock addr = %X\n", (uint32_t) (*r6));
		printf("lock wait addr = %X\n", (uint32_t) (*r28));
		printf("lock wait priv addr = %X\n", (uint32_t) (*r29));
		printf("lock barrier addr = %X\n", (uint32_t) (*r31));
		printf("clock = %ld\n", (uint64_t) (*clock));
		
	}
	
	
	if (proc_id == 110)
	{
		printf("main prog start detected\n");
	}
	
	/*
	  if (proc_id == 120)
	  {
	  printf("CPU0 reset");
	  }
	  
	  if (proc_id == 20)
	  {
	  printf("pc0 = %X\n", (uint32_t) (*pc));
	  printf("pc1 = %X\n", (uint32_t) (*r6));
	  printf("pc2 = %X\n", (uint32_t) (*r28));
	  printf("pc3 = %X\n", (uint32_t) (*r29));
	  printf("mutex lock addr = %X\n", (uint32_t) (*r31));
	  printf("clock = %d\n", clock);
	  }
	*/
	if (proc_id < core_number)
	{ 
		// we skip the multiple cycles instruction
		if(((uint64_t) (*clock) == (old_clock+1)) && (*pc==old_pc[proc_id]))
		{
			old_clock = (uint64_t) (*clock);
			old_pc[proc_id] = (uint32_t) (*pc);
			return 1;
		}
		
		old_clock = (uint64_t) (*clock);
		old_pc[proc_id] = (uint32_t) (*pc);
		
		
		data_signals[proc_id].pc = (uint32_t) (*pc);
		data_signals[proc_id].instr = (uint32_t) (*instr);
		data_signals[proc_id].r6 = (uint32_t) (*r6);
		data_signals[proc_id].r28 = (uint32_t) (*r28);
		data_signals[proc_id].r29 = (uint32_t) (*r29);
		data_signals[proc_id].r31 = (uint32_t) (*r31);
		data_signals[proc_id].clock = (uint64_t) (*clock);
		data_signals[proc_id].clock_bis = (uint64_t) (*clock_bis);
		
		printf("proc %d => clock %ld (clock bis %ld) : pc %X(%d) / r6 %X / r28 %X / r29 %X / r31 %X / instr %X\n", proc_id,
		       data_signals[proc_id].clock,
		       data_signals[proc_id].clock_bis,
		       data_signals[proc_id].pc, 
		       data_signals[proc_id].pc,
		       data_signals[proc_id].r6, data_signals[proc_id].r28, data_signals[proc_id].r29, 
		       data_signals[proc_id].r31,
		       data_signals[proc_id].instr);
	}
	return 1;
}
