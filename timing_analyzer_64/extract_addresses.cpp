#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string.h>

#include "structs.h"
#include "addr_extraction.h"


int main(int argc, char *argv[])
{
	struct addresses addr;
	memset(&addr, 0, sizeof(struct addresses));
	
	if (argc < 3)
	{
		printf("usage : %s <path to exe> <path to kernel image>\n", argv[0]);
		return 0;
	}

	std::ofstream addrFile;
	int i;
	addrFile.open ("extracted_addresses.txt");
	addrFile << "File containing all addresses to track\n";
	for (i=1; i<argc-1; i++)
	{
		addr = addr_extraction(argv[i], argv[argc-1]);
		addrFile << "#" << argv[i] << ":\n"; //exe name
		addrFile << "mutex_lock_addr:" << addr.mutex_lock_addr << "\n";
		addrFile << "mutex_unlock_addr:" << addr.mutex_unlock_addr << "\n";
		addrFile << "lock_wait_addr:" << addr.lock_wait_addr << "\n";
		addrFile << "lock_wait_priv_addr:" << addr.lock_wait_priv_addr << "\n";
		addrFile << "barrier_addr:" << addr.barrier_addr << "\n";
		addrFile << "barrier_wait_end_addr:" << addr.barrier_wait_end_addr << "\n";
		addrFile << "barrier_instr_futex_wake_addr:" << addr.barrier_instr_futex_wake_addr << "\n";
		addrFile << "main_start_addr:" << addr.main_start_addr << "\n";
		addrFile << "main_return_addr:" << addr.main_return_addr << "\n";
		addrFile << "ll_instr_addr:" << addr.ll_instr_addr << "\n";
		addrFile << "ll_instr_addr_2:" << addr.ll_instr_addr_2 << "\n";
		addrFile << "futex_wake:" << addr.futex_wake_addr << "\n";
		addrFile << "fct_uname_1:" << addr.fct_uname_1_addr << "\n";
		addrFile << "fct_uname_2:" << addr.fct_uname_2_addr << "\n";
		addrFile << "fct_uname_3:" << addr.fct_uname_3_addr << "\n";
		addrFile << "start_interest_section:" << addr.start_interest_section_addr << "\n";
		addrFile << "end_interest_section:" << addr.end_interest_section_addr << "\n";
		addrFile << "trig_instr_1:" << addr.trig_instr_1 << "\n";
		addrFile << "mask_instr_1:" << addr.mask_trig_instr_1 << "\n";
		addrFile << "trig_instr_2:" << addr.trig_instr_2 << "\n";
		addrFile << "mask_instr_2:" << addr.mask_trig_instr_2 << "\n\n";

		std::cout << "#" << argv[i] << ":\n"; //exe name
		std::cout << "mutex_lock_addr:" << std::hex << addr.mutex_lock_addr << "\n";
		std::cout << "mutex_unlock_addr:" << std::hex << addr.mutex_unlock_addr << "\n";
		std::cout << "lock_wait_addr:" << std::hex << addr.lock_wait_addr << "\n";
		std::cout << "lock_wait_priv_addr:" << std::hex << addr.lock_wait_priv_addr << "\n";
		std::cout << "barrier_addr:" << std::hex << addr.barrier_addr << "\n";
		std::cout << "barrier_wait_end_addr:" << std::hex << addr.barrier_wait_end_addr << "\n";
		std::cout << "barrier_instr_futex_wake_addr:" << std::hex << addr.barrier_instr_futex_wake_addr << "\n";
		std::cout << "main_start_addr:" << std::hex << addr.main_start_addr << "\n";
		std::cout << "main_return_addr:" << std::hex << addr.main_return_addr << "\n";
		std::cout << "ll_instr_addr:" << std::hex << addr.ll_instr_addr << "\n";
		std::cout << "ll_instr_addr_2:" << std::hex << addr.ll_instr_addr_2 << "\n";
		std::cout << "futex_wake:" << std::hex << addr.futex_wake_addr << "\n";
		std::cout << "fct_uname_1:" << addr.fct_uname_1_addr << "\n";
		std::cout << "fct_uname_2:" << addr.fct_uname_2_addr << "\n";
		std::cout << "fct_uname_3:" << addr.fct_uname_3_addr << "\n";
		std::cout << "start_interest_section:" << addr.start_interest_section_addr << "\n";
		std::cout << "end_interest_section:" << addr.end_interest_section_addr << "\n";
		std::cout << "trig_instr_1:" << addr.trig_instr_1 << "\n";
		std::cout << "mask_trig_instr_1:" << addr.mask_trig_instr_1 << "\n";
		std::cout << "trig_instr_2:" << addr.trig_instr_2 << "\n";
		std::cout << "mask_trig_instr_2:" << addr.mask_trig_instr_2 << "\n\n";
	}
	addrFile.close();
	
	return 1;
}
